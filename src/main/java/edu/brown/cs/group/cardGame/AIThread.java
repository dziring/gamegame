package edu.brown.cs.group.cardGame;

import edu.brown.cs.group.player.AIPlayer;

/**
 * Thread for running an AI player.
 *
 */
public class AIThread extends Thread {

  private Game currentGame;
  private AIPlayer player;

  /**
   * Constructs an AIThread given a game and a player.
   *
   * @param currentGame
   *          the game the AIPlayer is in
   * @param player
   *          the AIPlayer in question
   */
  public AIThread(Game currentGame, AIPlayer player) {
    super();
    this.currentGame = currentGame;
    this.player = player;
  }

  @SuppressWarnings("static-access")
  @Override
  public void run() {
    while (!currentGame.isGameOver()) {
      if (currentGame.getWhoseTurn() == player.getID()) {
        if (currentGame.callsTricks()) {
          Game currentTrickGame = currentGame;
          if (!currentTrickGame.getTricksCalled().containsKey(player.getID())) {
            if (!currentTrickGame.callTricks(player,
                player.makeBet(currentGame))) {
              if (!currentTrickGame.callTricks(player, 0)) {
                currentTrickGame.callTricks(player, 2);
              }

            }
          }

        }
        if (player.getHand().size() > 0) {
          if (!currentGame.playCard(new PlayerCard(player, player
              .generateMove(currentGame)))) {
            for (int i = 0; i < player.getHand().size(); i++) {
              if (currentGame.playCard(new PlayerCard(player, player.getHand()
                  .get(i)))) {
                break;
              }
            }

          }

        }
      }
      try {
        this.sleep(5000L);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

}
