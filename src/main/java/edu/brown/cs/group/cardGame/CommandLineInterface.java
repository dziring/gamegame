package edu.brown.cs.group.cardGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.brown.cs.group.player.HumanPlayer;
import edu.brown.cs.group.player.Player;

/**
 * A command line interface used for testing our card game implementations.
 *
 */
public class CommandLineInterface {

  public static void main(String[] args) throws InterruptedException {
    run();
  }

  public static void printOptions() {
    System.out.println("Options: ");
    System.out.println("'rules' -- Shows the game rules");
    System.out.println("'print pot' -- Shows the cards currently on the table");
    System.out
        .println("'print hand' -- Prints the cards in the current player's hand");
    System.out.println("'print trump' -- Prints the trump suit in the game");
    System.out
        .println("'play <#>' -- Plays the #-indexed card from the current player's hand");
    System.out
        .println("'print scores' -- Prints the overall scores of all players in the game");
    System.out.println("'help' -- Prints this options list");
    System.out.println("'exit' -- Quits the program");
  }

  /**
   * This method runs the command line interface.
   * 
   * @throws InterruptedException
   *           if the thread is interrupted.
   */
  public static void run() throws InterruptedException {
    BufferedReader scanner = new BufferedReader(
        new InputStreamReader(System.in));
    String input = "";
    String game = "";
    try {
      while (true) {
        System.out
            .println("Choose 'hearts' or 'spades' or 'generic' or 'setback'");
        input = scanner.readLine();
        if (input.equalsIgnoreCase("hearts")) {
          game = "hearts";
          break;
        } else if (input.equals("spades")) {
          game = "spades";
          break;
        } else if (input.equals("generic")) {
          game = "generic";
          break;
        } else if (input.equals("setback")) {
          game = "setback";
          break;
        }
      }
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    // Create players.
    Player player0 = new HumanPlayer("Player 0");
    Player player1 = new HumanPlayer("Player 1");
    Player player2 = new HumanPlayer("Player 2");
    Player player3 = new HumanPlayer("Player 3");
    player0.setID(0);
    player1.setID(1);
    player2.setID(2);
    player3.setID(3);
    Map<Integer, Player> players = new HashMap<Integer, Player>();
    players.put(0, player0);
    players.put(1, player1);
    players.put(2, player2);
    players.put(3, player3);
    // Create the game on its own thread.
    Game myGame = null;
    if (game.equals("hearts")) {
      myGame = new Hearts(players, "hearts");
    } else if (game.equals("spades")) {
      myGame = new Spades(players, "spades");
    } else if (game.equals("generic")) {
      List<Integer> scoring = new ArrayList<Integer>(Game.CARDS_IN_DECK);
      for (int i = 0; i < Game.CARDS_IN_DECK; i++) {
        if (i < Game.CARDS_IN_SUIT - 1)
          scoring.add(i + 2);
        else if (i % Game.CARDS_IN_SUIT == Game.CARDS_IN_SUIT - 1)
          scoring.add(14);
        else
          scoring.add(0);
      }
      myGame = new GenericGame(false, scoring, false, 100, true, 3, null,
          false, "generic-cmd", players, new ArrayList<Integer>(), false);
    } else if (game.equals("setback")) {
      myGame = new Setback(players, "setback-cmd");
    }

    myGame.startGame();

    // Set up logic for switching players in turns.
    Player currentTurnPlayer = players.get(myGame.getWhoseTurn());
    boolean switchToNextPlayer = false;
    boolean optionsPrinted = false;
    boolean winnerPrinted = true;
    while (input != null) {
      if (switchToNextPlayer) {
        currentTurnPlayer = myGame.getPlayerList().get(myGame.getWhoseTurn());
        Thread.sleep(100);
        currentTurnPlayer = myGame.getPlayerList().get(myGame.getWhoseTurn());
      }
      switchToNextPlayer = false;
      try {
        if (game.equalsIgnoreCase("spades") || game.equalsIgnoreCase("setback")) {
          currentTurnPlayer = players.get(myGame.getWhoseTurn());
          boolean tricksCalledIsNotFour = false;
          if (game.equalsIgnoreCase("spades")) {
            tricksCalledIsNotFour = ((Spades) myGame).getTricksCalled().size() != 4;
          } else if (game.equalsIgnoreCase("setback")) {
            tricksCalledIsNotFour = ((Setback) myGame).getTricksCalled().size() != 4;
          }
          if (tricksCalledIsNotFour) {
            System.out.println("Pick tricks!");
            System.out.println("Type 'print hand' to see your cards");
            int start = currentTurnPlayer.getID();
            for (int x = start; x < start + 4; x++) {
              while (true) {
                System.out.println("Player " + x % 4
                    + ", how many tricks/points? ");
                input = scanner.readLine();
                if (input.equals("print hand")) {
                  for (int i = 0; i < currentTurnPlayer.getHand().size(); i++) {
                    System.out.println(i + ")"
                        + currentTurnPlayer.getHand().get(i));
                  }
                } else {
                  try {
                    int numtricks = Integer.parseInt(input);
                    if (myGame.callTricks(myGame.getPlayers().get(x % 4),
                        numtricks)) {
                      break;
                    } else {
                      System.out
                          .println("Pick your tricks/points correctly please.");
                    }
                  } catch (NumberFormatException e) {
                  }
                }
              }
            }
          }
        }
        currentTurnPlayer = players.get(myGame.getWhoseTurn());
        Thread.sleep(150);
        List<PlayerCard> thePot = myGame.getPot();
        if (!optionsPrinted) {
          System.out.println();
          printOptions();
          System.out.println();
          optionsPrinted = true;
        } else if (!winnerPrinted) {
          System.out.println("***" + currentTurnPlayer
              + " won the previous hand.***");
          winnerPrinted = true;
        }

        System.out.println("Pot: " + thePot);
        System.out.println("It is " + currentTurnPlayer + "'s turn.");
        input = scanner.readLine();

        if (input.equals("help")) {
          printOptions();
        } else if (input.equals("rules")) {
          System.out.println(myGame.getRules());
        } else if (input.equals("print trump")) {
          System.out.println(myGame.getTrump());
        } else if (input.equals("print pot")) {
          thePot = myGame.getPot();
          System.out.println(thePot + "\n");
        } else if (input.equals("print hand")) {
          for (int x = 0; x < currentTurnPlayer.getHand().size(); x++) {
            System.out.println(x + ") " + currentTurnPlayer.getHand().get(x));
          }
          System.out.println();
        } else if (input.startsWith("play")) {
          String[] split = input.split(" ");
          if (split.length == 2) {
            int cardNumber = Integer.parseInt(split[1]);
            if (cardNumber < 0
                || cardNumber >= currentTurnPlayer.getHand().size()) {
              System.out.println("ERROR: Not a valid card number");
            } else {
              if (myGame.getPot().size() == 3) {
                winnerPrinted = false;
              }
              PlayerCard oneTurn = new PlayerCard(currentTurnPlayer,
                  currentTurnPlayer.getHand().get(cardNumber));
              if (myGame.playCard(oneTurn)) {
                System.out.println(currentTurnPlayer + " played "
                    + oneTurn.getCard() + "\n");
                switchToNextPlayer = true;
              } else {
                System.out.println("This is not a valid move. Try again. \n");
                winnerPrinted = true;
              }
            }
          } else {
            System.out.println("ERROR: Not a valid command/card number");
          }
        } else if (input.equals("print scores")) {
          for (Player p : myGame.getPlayerList()) {
            System.out.print(p + ":" + p.getScore() + "," + "\n");
          }
        } else if (input.equals("exit")) {
          System.exit(0);
        }

        if (myGame.isGameOver()) {
          System.out.println("Game over! " + myGame.getWinner()
              + " is the winner");
          for (Player p : myGame.getPlayerList()) {
            System.out.print(p + ":" + p.getScore() + "," + "\n");
          }
        }

      } catch (IOException | NumberFormatException | InterruptedException e) {
        System.out.println("ERROR: " + e.getMessage());
      }
    }
  }
}
