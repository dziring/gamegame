package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.messages.Messenger;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.Player;

/**
 * An abstract class that represents a cards trick calling Game.
 */
public abstract class Game {

  private Map<Integer, Player> players;
  private String name;
  private String info;
  private int whoseTurn;
  private boolean callsTricks;
  protected boolean hasTeams = false;
  protected Map<Integer, Integer> tricksCalled;

  private Messenger msn;

  private long lastChanged;

  public static final int NUM_PLAYERS = 4;
  public static final int CARDS_IN_DECK = 52;
  public static final int CARDS_IN_SUIT = 13;

  public static final List<String> WINNER_STRINGS = ImmutableList.of(
      "%s is in the lead with a score of %d.", "%s is winning with %d points",
      "Everyone is behind %s, who has %d points.");
  public static final List<String> TWO_WAY_TIES = ImmutableList.of(
      "%s and %s are tied at %d!", "%s is neck-and-neck with %s at %d.",
      "%s is tied with %s for first with %d points.");
  public static final List<String> THREE_WAY_TIES = ImmutableList.of(
      "%s, %s, and %s are tied at %d.",
      "It's a three-way tie between %s, %s, and %s at %d.");
  public static final List<String> ALL_TIES = ImmutableList
      .of("Everyone is tied at %d.");
  public static final String NEW_ROUND = "A new round has started!";

  private List<PlayerCard> pot;
  private List<PlayerCard> previousPot;
  private int previousWinner;
  private Suit currentOnSuit;
  private Suit trumpSuit;

  // True if lowest score wins, false if highest score wins
  private boolean reverseScoring;

  /**
   * Constructs a new game with no players and no name.
   */
  public Game() {
    this(new HashMap<Integer, Player>(), "");
  }

  /**
   * Constructs a game with no players.
   * 
   * @param name
   *          the name of the game.
   */
  public Game(String name) {
    this(new HashMap<Integer, Player>(), name);
  }

  /**
   * Constructs a game.
   *
   * @param players
   *          a map of playerNumner to the players. (The playerNumbers must
   *          range between 0 and 3, inclusive). This is because there can only
   *          be four player in this game.
   * @param name
   *          the name of the game.
   */
  public Game(Map<Integer, Player> players, String name) {
    this.players = players;
    this.whoseTurn = 0;
    this.name = name;
    this.info = "";
    this.reverseScoring = false;
    this.trumpSuit = null;
    this.msn = new Messenger();
    this.callsTricks = false;
    this.lastChanged = 0L;
    this.previousWinner = -1;
  }

  /**
   * Updates the timestamp on this game. Should be called: Whenever a card is
   * played successfully A player joins the game The round is reset
   */
  protected void stateChanged() {
    this.lastChanged = System.currentTimeMillis();
  }

  /**
   * @return a unix timestamp for the last time this game was changed
   */
  public long getLastChanged() {
    return this.lastChanged;
  }

  /**
   * Sets the scoring of the game.
   * 
   * @param reverse
   *          true if the lowest score wins, false if the highest score wins.
   */
  public void setReverseScoring(boolean reverse) {
    this.reverseScoring = reverse;
  }

  /**
   * Gets the trump suit of the game.
   * 
   * @return the suit of the trump suit.
   */
  public Suit getTrump() {
    return trumpSuit;
  }

  /**
   * Sets the trump suit of the game.
   * 
   * @param trump
   *          the suit that is to be set as the trump suit.
   */
  public void setTrump(Suit trump) {
    this.trumpSuit = trump;
  }

  /**
   * Gets the current on suit of the round.
   * 
   * @return the suit the represents the current on suit of the round.
   */
  public Suit getCurrentOnSuit() {
    return currentOnSuit;
  }

  /**
   * Sets the current on suit of the round.
   * 
   * @param current
   *          the suit that represents the current on suit of the round.
   */
  public void setCurrentOnSuit(Suit current) {
    this.currentOnSuit = current;
  }

  /**
   * Returns a list of all players in the game.
   *
   * @return A list of all players in the game, in no particular order.
   */
  public List<Player> getPlayerList() {
    List<Player> playerList = new ArrayList<Player>();
    for (int num : getPlayers().keySet()) {
      playerList.add(getPlayers().get(num));
    }
    return playerList;
  }

  /**
   * Returns the number of players in the game currently.
   *
   * @return the int representing the number of players in the game.
   */
  public int numPlayers() {
    return getPlayers().size();
  }

  /**
   * Currently basically the same as run and we'll choose whichever one is
   * better.
   */
  public abstract void startGame();

  /**
   * Deals the cards to the players.
   */
  public abstract void dealCards();

  /**
   * Evaluates the pot after all players have played their cards for the one
   * hand.
   *
   * @param player
   *          the list of PlayerCard's associated with this hand that are in the
   *          pot.
   * @return the player that won the particular hand.
   */
  public abstract Player evaluatePot(List<PlayerCard> player);

  /**
   * Plays a card for the current ongoing hand.
   *
   * @param turn
   *          a PlayerCard that has the player and their associated turn.
   * @return a boolean representing whether the move is valid or not, true if it
   *         is valid.
   */
  public abstract boolean playCard(PlayerCard turn);

  /**
   * A method that determines if a particular turn by a player is valid.
   *
   * @param turn
   *          a PlayerCard that has the player and their associated turn.
   * @return a boolean representing whether the move is valid or not, true is it
   *         is valid.
   */
  public abstract boolean isValidMove(PlayerCard turn);

  /**
   * Gets the winner of the game if the game is over.
   *
   * @return the player in the game who won. Throws an Illegal State Exception
   *         if the game is not over.
   */
  public abstract Player getWinner();

  /**
   * Determines if the game is over.
   *
   * @return true if the game is over, false otherwise.
   */
  public abstract boolean isGameOver();

  /**
   * Returns an explanation of the rules of the game.
   *
   * @return an explanation of the gameplay
   */
  public abstract String getRules();

  /**
   * Determines if the game is ready to be played.
   *
   * @return True if the number of players in the game is 4, and false otherwise
   */
  public boolean isReady() {
    return players.size() == 4;
  }

  /**
   * Adds a new player to the game.
   *
   * @param newPlayer
   *          The player to be added to the game.
   * @return newPlayer's game ID.
   */
  public int addPlayer(Player newPlayer) {
    int id = -1;
    if (!players.containsKey(0)) {
      id = 0;
    } else if (!players.containsKey(1)) {
      id = 1;
    } else if (!players.containsKey(2)) {
      players.put(2, newPlayer);
      id = 2;
    } else if (!players.containsKey(3)) {
      id = 3;
    }
    if (id != -1) {
      players.put(id, newPlayer);
      newPlayer.setID(id);
      stateChanged();
      return id;
    }
    return -1;
  }

  /**
   * Adds a new player to the game at a given id.
   *
   * @param newPlayer
   *          The new player to be added to the game
   * @param id
   *          newPlayer's id for this game
   * @return newPlayer's ID
   */
  public int addPlayer(Player newPlayer, int id) {
    players.put(id, newPlayer);
    newPlayer.setID(id);
    return id;
  }

  /**
   * Returns a map of the the players number and the player itself.
   *
   * @return Returns a map of the the players number and the player itself. This
   *         will range from the number 0 to 3 (inclusive) as there are only
   *         four people allowed to play in a game.
   */
  public Map<Integer, Player> getPlayers() {
    return ImmutableMap.copyOf(players);
  }

  /**
   * Gets the name of the game.
   *
   * @return this.name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of the game.
   *
   * @param newName
   *          The new name of the game.
   */
  public void setName(String newName) {
    this.name = newName;
  }

  /**
   * Determines whether the players have cards.
   * 
   * @return true if the players still have cards in their hands, false
   *         otherwise.
   */
  public boolean playersHaveCards() {
    for (Player player : players.values()) {
      if (player.hasCards()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a card to the pot.
   * 
   * @param turn
   *          a PlayerCard that represents a turn.
   */
  public void addToPot(PlayerCard turn) {
    this.pot.add(turn);
  }

  protected void updateInfo(boolean reverse) {
    Map<Integer, Player> pm = this.getPlayers();
    List<Player> highests = new ArrayList<Player>();
    int highscore = pm.get(0).getScore();
    if (reverse) {
      highscore = -highscore;
    }
    for (Integer i : pm.keySet()) {
      int score = pm.get(i).getScore();
      if (reverse) {
        score = -score;
      }
      if (score > highscore) {
        highests.clear();
        highests.add(pm.get(i));
        highscore = score;
      } else if (score == highscore) {
        highests.add(pm.get(i));
      }
    }

    String state = winnerString(highests);
    Object[] args = new Object[highests.size() + 1];
    for (int i = 0; i < highests.size(); i++) {
      args[i] = highests.get(i).getName();
    }
    args[highests.size()] = reverse ? -highscore : highscore;
    if (highests.size() != 4) {
      this.info = String.format(state, args);
    } else {
      this.info = String.format(state, reverse ? -highscore : highscore);
    }

    if (trumpSuit != null) {
      this.info = this.info + " The trump suit is " + trumpSuit;
    }

    if (!playersHaveCards()) {
      this.info = "It's a new round!";
    }
  }

  private static <T> T pickOne(List<T> things) {
    int index = (new Random()).nextInt(things.size());
    return things.get(index);
  }

  private static String winnerString(List<Player> l) {
    assert !l.isEmpty();

    List<String> toChoose = null;
    switch (l.size()) {
    case 1:
      toChoose = WINNER_STRINGS;
      break;
    case 2:
      toChoose = TWO_WAY_TIES;
      break;
    case 3:
      toChoose = THREE_WAY_TIES;
      break;
    case 4:
      toChoose = ALL_TIES;
      break;
    default:
      toChoose = ALL_TIES;
    }

    return pickOne(toChoose);
  }

  /**
   * Returns the current pot of the game.
   *
   * @return A list of playerCard's which represent the player and their card
   *         played currently in the pot.
   */
  public List<PlayerCard> getPot() {
    return ImmutableList.copyOf(pot);
  }

  public void setPot(List<PlayerCard> pot) {
    this.pot = pot;
  }

  /**
   * Stores the players trick for the game if relevant.
   * 
   * @param player
   *          the player who calls the trick.
   * @param tricks
   *          the number of tricks called by the player.
   * @return true if the trick is valid, false otherwise.
   */
  public boolean callTricks(Player player, int tricks) {
    return false;
  }

  /**
   * Gets whose turn it is in the game.
   * 
   * @return the int corresponding to the player whose turn it is (between 0 and
   *         3 inclusive).
   */
  public int getWhoseTurn() {
    return whoseTurn;
  }

  /**
   * Sets whose turn it is in the game.
   * 
   * @param turn
   *          the number corresponding to the player whose turn it is (between 0
   *          and 3 inclusive).
   */
  public void setWhoseTurn(int turn) {
    whoseTurn = turn;
    stateChanged();
  }

  /**
   * Gets the previous pot of the game.
   * 
   * @return the previous pot of the game (a List of PlayerCard's).
   */
  public ImmutableList<PlayerCard> getPreviousPot() {
    return ImmutableList.copyOf(previousPot);
  }

  protected void resetRound() {

    try {
      Thread.sleep(1000L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    updateInfo(reverseScoring);
    currentOnSuit = null;
    previousPot = ImmutableList.copyOf(pot);
    pot.clear();
    stateChanged();
    setPreviousWinner(getWhoseTurn());
  }

  /**
   * Replaces a player with an AI player (should be called when a player leaves
   * the game for any reason).
   *
   * @param p
   *          The player who left the game
   */
  public AIPlayer replaceWithAIPlayer(Player p) {
    List<Card> newHand = new ArrayList<Card>();
    for (int i = 0; i < p.getHand().size(); i++) {
      Card newCard = new Card(p.getHand().get(i).getRank(), p.getHand().get(i)
          .getSuit());
      newHand.add(newCard);
    }

    AIPlayer newPlayer = new AIPlayer(p.getName(), p.getID(), newHand,
        p.getScore());
    players.put(newPlayer.getID(), newPlayer);
    return newPlayer;
  }

  /**
   * @return the messenger object for this game
   */
  public Messenger getMessenger() {
    return this.msn;
  }

  /**
   * Returns if this game calls tricks or not.
   *
   * @return this.callsTricks
   */
  public boolean callsTricks() {
    return callsTricks;
  }
  
  public Map<Integer, Integer> getTricksCalled() {
    return ImmutableMap.copyOf(tricksCalled);
  }
  
  /**
   * Sets if the game is a trick-calling game in child classes.
   *
   * @param callsTricks
   *          True if the game is a trick calling game and false otherwise
   */
  protected void setCallsTricks(boolean callsTricks) {
    this.callsTricks = callsTricks;
    tricksCalled = new HashMap<Integer, Integer>();
  }
  
  protected boolean getCallsTricks() {
    return this.callsTricks;
  }

  protected void setHasTeams(boolean teams) {
    this.hasTeams = teams;
  }

  protected void setPreviousWinner(int playerID) {
    this.previousWinner = playerID;
  }

}
