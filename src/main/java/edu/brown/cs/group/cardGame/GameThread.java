package edu.brown.cs.group.cardGame;

/**
 * Class that extends Thread for running a card game.
 *
 */
public class GameThread extends Thread {

  private Game game;

  /**
   * Constructs a GameThread.
   *
   * @param game the game that the thread must run.
   */
  public GameThread(Game game) {
    this.game = game;
  }

  @Override
  public void run() {
    game.startGame();
  }
}
