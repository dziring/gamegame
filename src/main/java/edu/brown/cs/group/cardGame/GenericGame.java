package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.Deck;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.GenericGameAIPlayer;
import edu.brown.cs.group.player.Player;

/**
 * Constructs a generic game, where you can build your own rules.
 *
 */
public class GenericGame extends Game {

  public static final int NUM_PLAYERS = 4;

  private boolean isMaxPoints;
  private List<Integer> scoringMethod;
  private boolean endsByRound;
  private int endGamePoint;
  private int numRoundsPlayed;
  private boolean isAceHigh;
  private int cardsInHand;
  private boolean isTrumpBroken;
  private boolean playingWithTrumpBroken;
  private List<Integer> excludeCards;
  private Map<Player, Integer> currentRoundScores;
  private int tricksover;
  private int tricksmissed;
  private int trickscalled;

  /**
   * Constructs a generic game.
   * 
   * @param isMaxPoints
   * @param scoringMethod
   * @param endsByRound
   * @param endGamePoint
   * @param isAceHigh
   * @param cardsInHand
   * @param trumpSuit
   * @param callTricks
   * @param name
   * @param players
   */
  public GenericGame(boolean isMaxPoints, List<Integer> scoringMethod,
      boolean endsByRound, int endGamePoint, boolean isAceHigh,
      int cardsInHand, Suit trumpSuit, boolean callTricks, String name,
      Map<Integer, Player> players, List<Integer> excludeCards,
      boolean trumpBroken) {
    super(players, name);
    this.isMaxPoints = isMaxPoints;
    this.scoringMethod = scoringMethod;
    this.endsByRound = endsByRound;
    this.endGamePoint = endGamePoint;
    setTrump(trumpSuit);
    setCallsTricks(callTricks);
    currentRoundScores = new HashMap<Player, Integer>();
    this.isAceHigh = isAceHigh;
    this.cardsInHand = cardsInHand;
    this.numRoundsPlayed = 0;
    this.excludeCards = excludeCards;
    this.playingWithTrumpBroken = trumpBroken;
    setPot(new ArrayList<PlayerCard>());
  }

  /**
   * Constructs a generic game.
   * 
   * @param isMaxPoints
   * @param scoringMethod
   * @param endsByRound
   * @param endGamePoint
   * @param isAceHigh
   * @param cardsInHand
   * @param trumpSuit
   * @param callTricks
   * @param name
   */
  public GenericGame(boolean isMaxPoints, List<Integer> scoringMethod,
      boolean endsByRound, int endGamePoint, boolean isAceHigh,
      int cardsInHand, Suit trumpSuit, boolean callTricks, String name,
      List<Integer> excludeCards, boolean trumpBroken) {
    this(isMaxPoints, scoringMethod, endsByRound, endGamePoint, isAceHigh,
        cardsInHand, trumpSuit, callTricks, name,
        new HashMap<Integer, Player>(), excludeCards, trumpBroken);
  }

  /**
   * Constructs a generic game.
   * 
   * @param isMaxPoints
   * @param scoringMethod
   * @param endsByRound
   * @param endGamePoint
   * @param isAceHigh
   * @param cardsInHand
   * @param trumpSuit
   * @param callTricks
   */
  public GenericGame(boolean isMaxPoints, List<Integer> scoringMethod,
      boolean endsByRound, int endGamePoint, boolean isAceHigh,
      int cardsInHand, Suit trumpSuit, boolean callTricks,
      List<Integer> excludeCards, boolean trumpBroken) {
    this(isMaxPoints, scoringMethod, endsByRound, endGamePoint, isAceHigh,
        cardsInHand, trumpSuit, callTricks, "", new HashMap<Integer, Player>(),
        excludeCards, trumpBroken);
  }

  @Override
  public void dealCards() {
    Deck deck = new Deck(isAceHigh, excludeCards);
    int currPlayer = 0;
    int dealt = 0;
    while (Math.floor(dealt / NUM_PLAYERS) < cardsInHand) {
      getPlayers().get(currPlayer).addCard(deck.draw());
      currPlayer = (currPlayer + 1) % NUM_PLAYERS;
      dealt++;
    }
  }

  @Override
  public Player evaluatePot(List<PlayerCard> player) {
    PlayerCard highest = getPot().get(0);
    for (PlayerCard pc : getPot()) {
      // Played card is trump
      if (pc.getCard().getSuit().equals(getTrump())) {
        if (!highest.getCard().getSuit().equals(getCurrentOnSuit())) {
          highest = pc;
          continue;
        }
        // Both cards are trump
        if (Rank.compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
        // Neither card is trump
      } else if (!highest.getCard().getSuit().equals(getTrump())) {
        if (pc.getCard().getSuit().equals(getCurrentOnSuit())
            && Rank
                .compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
      }
    }
    if (!getCallsTricks()) {
      for (PlayerCard pc : getPot()) {
        highest.getPlayer().addToScore(
            scoringMethod.get(Card.getScoringIndex(pc.getCard())));
      }
    }

    return highest.getPlayer();
  }

  protected void resetRound() {
    if (!getPot().isEmpty()) {
      Player winner = evaluatePot(getPot());
      setCurrentOnSuit(null);
      setWhoseTurn(winner.getID());
      this.getMessenger().log(
          getPlayers().get(getWhoseTurn()).getName() + " took the pot.");
      super.resetRound();
      if (getCallsTricks()) {
        currentRoundScores.put(winner, currentRoundScores.get(winner) + 1);
      }
    }
    if (!getPlayers().get(0).hasCards()) {
      if (getCallsTricks()) {
        updateScores(currentRoundScores);
      }
      if (!isGameOver()) {
        numRoundsPlayed++;
        if (playingWithTrumpBroken) {
          isTrumpBroken = false;
        } else {
          isTrumpBroken = true;
        }
        currentRoundScores = new HashMap<Player, Integer>();
        for (Player player : getPlayerList()) {
          currentRoundScores.put(player, 0);
        }
        tricksCalled = new HashMap<Integer, Integer>();
        dealCards();
        setPot(new ArrayList<PlayerCard>());
        this.stateChanged();
      }
    }
  }

  private void updateScores(Map<Player, Integer> roundScores) {
    for (Player player : roundScores.keySet()) {
      int called = tricksCalled.get(player.getID());
      int gotten = roundScores.get(player);
      if (called > gotten) {
        player.addToScore(-called * tricksmissed);
      } else {
        int tricksOver = gotten - called;
        player.addToScore(called * trickscalled + tricksOver * tricksover);

      }
    }
  }

  public void setTricksMissed(int tricksmissed) {
    this.tricksmissed = tricksmissed;
  }

  public void setTricksCalled(int trickscalled) {
    this.trickscalled = trickscalled;
  }

  public void setTricksOver(int tricksover) {
    this.tricksover = tricksover;
  }

  /**
   * Sets whether this game should be played with breaking trump.
   * 
   * @param playingWithTrump
   *          true if playing with trump broken rule, false otherwise.
   */
  public void setPlayingWithTrumpBroken(boolean playingWithTrump) {
    this.playingWithTrumpBroken = playingWithTrump;
  }

  @Override
  public boolean playCard(PlayerCard turn) {
    if (isValidMove(turn)) {
      if (getPot().isEmpty()) {
        setCurrentOnSuit(turn.getCard().getSuit());
      }
      if (turn.getCard().getSuit().equals(getTrump())) {
        isTrumpBroken = true;
      }
      addToPot(turn);
      turn.getPlayer().removeCard(turn.getCard());
      setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
      this.getMessenger().log(
          turn.getPlayer().getName() + " played " + turn.getCard().toString());
      if (getPot().size() == NUM_PLAYERS) {
        resetRound();
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean callTricks(Player player, int tricks) {
    if (getWhoseTurn() != player.getID()) {
      return false;
    }
    if (tricks < 0 || tricks > cardsInHand || tricksCalled.containsKey(player)) {
      return false;
    }

    setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
    tricksCalled.put(player.getID(), tricks);
    int playerWithMaxTricks = 0;
    if (tricksCalled.size() == NUM_PLAYERS) {
      for (int curPlayer : tricksCalled.keySet()) {
        int curTricks = tricksCalled.get(curPlayer);
        if (curTricks > playerWithMaxTricks) {
          playerWithMaxTricks = curPlayer;
        }
      }
      setWhoseTurn(playerWithMaxTricks);
    }
    return true;
  }

  @Override
  public boolean isValidMove(PlayerCard turn) {
    if (tricksCalled.size() < NUM_PLAYERS && getCallsTricks()) {
      return false;
    }
    if (getWhoseTurn() != turn.getPlayer().getID()) {
      return false;
    }
    for (PlayerCard currentTurn : getPot()) {
      if (currentTurn.getPlayer().equals(turn.getPlayer())) {
        return false;
      }
    }
    if (getPot().isEmpty()) {
      setCurrentOnSuit(turn.getCard().getSuit());
      if (turn.getCard().getSuit().equals((getTrump()))) {
        if (isTrumpBroken()) {
          return true;
        } else if (turn.getPlayer().numberOfSuit(getTrump()) == turn
            .getPlayer().handSize()) {
          return true;
        } else {
          return false;
        }
      }
    } else if (turn.getPlayer().hasSuit(getCurrentOnSuit())
        && (!turn.getCard().getSuit().equals(getCurrentOnSuit()))) {
      return false;
    }
    return true;
  }

  public boolean getIsMaxPoints() {
    return isMaxPoints;
  }

  /**
   * Specifies if trump has been broken yet.
   *
   * @return True if trump has been broken and false otherwise
   */
  public boolean isTrumpBroken() {
    return isTrumpBroken;
  }

  @Override
  public Player getWinner() {
    if (!isGameOver()) {
      throw new IllegalStateException("Game is not over");
    }
    Player winner = getPlayers().get(0);
    int coef = isMaxPoints ? -1 : 1;
    for (Player player : getPlayers().values()) {
      if (player.getScore() * coef < winner.getScore() * coef) {
        winner = player;
      }
    }
    return winner;
  }

  @Override
  public boolean isGameOver() {
    if (endsByRound && numRoundsPlayed == endGamePoint) {
      return true;
    } else if (!endsByRound) {
      for (Player player : getPlayers().values()) {
        if (player.getScore() >= endGamePoint) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void startGame() {
    resetRound();
  }

  @Override
  public AIPlayer replaceWithAIPlayer(Player p) {
    AIPlayer newPlayer = new GenericGameAIPlayer(p.getName(), p.getID(),
        p.getHand(), p.getScore());
    addPlayer(newPlayer, newPlayer.getID());
    return newPlayer;
  }

  @Override
  public String getRules() {
    StringBuilder sb = new StringBuilder();
    String title = "You are currently playing " + getName() + ".<br><br>";
    String goal = "GOAL: <br>Get the " + (isMaxPoints ? "highest" : "lowest")
        + " number of total points.<br><br>";
    String points = "";
    if (!getCallsTricks()) {
      points = "POINT CARDS:<br>";
      boolean cardsSet = false;
      for (int i = 0; i < 52; i++) {
        if (scoringMethod.get(i) != 0) {
          cardsSet = true;
          boolean allEqual = true;
          for (int j = 0; j < 4; j++) {
            if (scoringMethod.get(i % 13) != scoringMethod.get(i % 13 + j * 13)) {
              allEqual = false;
            }
          }
          if (allEqual) {
            if (i < 13) {
              points += "All " + Card.getCardFromIndex(i).getRank() + "S are "
                  + "worth " + scoringMethod.get(i) + " points.<br>";
            }
            continue;
          }

          points += "The " + Card.getCardFromIndex(i).getRank() + " of "
              + Card.getCardFromIndex(i).getSuit() + " is worth "
              + scoringMethod.get(i) + " points.<br>";
        }
      }
      points += "All " + (cardsSet ? "other" : "")
          + " cards are worth 0 points.<br><br>";
    } else {
      points += "SCORING POINTS:<br>";
      points += "You score " + trickscalled
          + " per trick correctly called. <br>";
      points += "You score " + tricksmissed
          + " per trick called, if you undershoot. <br>";
      points += "You score " + tricksover
          + " per trick you win over the number you called. <br><br>";
    }
    String validMoves = "VALID MOVES: <br>On your turn, you must play a card that is \"on suit\","
        + " meaning that it matches the first card played in that round. If you don't have that"
        + " suit, you can play any other card in your hand. <br><br>";
    String trumpLine = "";
    if (getTrump() != null) {
      trumpLine = "The player who has the highest card of "
          + getTrump()
          + " in the previous"
          + " round starts the next round; if no trump cards were played, the player who has the highest"
          + "\"on suit\" card in the previous round starts the next round.<br>";
    } else {
      trumpLine = "The player who has the highest "
          + "\"on suit\" card in the previous round starts the next round.<br>";
    }

    String gamePlay = "GAMEPLAY:<br> "
        + trumpLine
        + " The game continues until "
        + (endsByRound ? endGamePoint + " rounds have been played"
            : "a player gets " + endGamePoint + " points")
        + ", at which point the player with the "
        + (isMaxPoints ? "highest" : "lowest") + " points wins.<br>"
        + "Aces in this game are " + (isAceHigh ? "high" : "low")
        + ", and each player starts each hand with " + cardsInHand
        + " cards in their hand.";
    sb.append(title);
    sb.append(goal);
    sb.append(points);
    sb.append(validMoves);
    sb.append(gamePlay);
    return sb.toString();
  }
}
