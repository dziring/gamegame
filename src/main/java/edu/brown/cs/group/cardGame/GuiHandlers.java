package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import spark.ModelAndView;
import spark.QueryParamsMap;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.TemplateViewRoute;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.gson.Gson;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.messages.Message;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.GenericGameAIPlayer;
import edu.brown.cs.group.player.HeartsAIPlayer;
import edu.brown.cs.group.player.HumanPlayer;
import edu.brown.cs.group.player.Player;
import edu.brown.cs.group.player.SetbackAIPlayer;
import edu.brown.cs.group.player.SpadesAIPlayer;

public final class GuiHandlers {

  private static final Gson GSON = new Gson();

  /*
   * HTTP status codes
   * 
   * 304 -- Not Modified: Returned when the client makes a conditional request
   * for a resource and the server wants to indicate that it has not
   * changed--the client's copy is still correct 204 -- No Content: Returned
   * when a request was good, but there will be no content returned in the
   * response. E.g., playing your turn on a game. 403 -- Forbidden: Pretty
   * self-explanatory.
   */
  public static final int SERVER_ERROR = 500;
  public static final int NOT_MODIFIED = 304;
  public static final int NO_CONTENT = 204;
  public static final int OK = 200;
  public static final int BAD_REQUEST = 400;
  public static String[] hardAIPlayers = new String[] { "Mike", "Gabe",
      "Jessica", "Vinh", "JJ" };
  public static String[] easyAIPlayers = new String[] { "Indy", "Aisha", "Eli",
      "Emma", "DJ", "Ping", "Andrew" };

  /**
   * General JSON information about a particular game.
   *
   * @author carlos
   */
  public static class GameUpdate implements Route {
    private Map<String, Game> games;

    public GameUpdate(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      /*
       * TODO if we want to do this right, should have the frontend pass in a
       * timestamp or version number, and give them back 304 Not Modified if
       * there has been no change since then.
       */
      QueryParamsMap qm = req.queryMap();
      String id = req.params("id");

      Game currentGame = games.get(id);

      if (currentGame == null) {
        res.redirect("/home");
        return null;
      }

      String timestamp = qm.value("t");
      long time = (timestamp != null) ? Long.valueOf(timestamp) : 0;

      if (currentGame.getLastChanged() <= time) {
        res.status(NOT_MODIFIED);
        return "";
      }

      Builder<String, Object> b = new ImmutableMap.Builder<>();
      b.put("game", currentGame);
      b.put("time", System.currentTimeMillis() / 1000L);
      b.put("isOver", currentGame.isGameOver());
      if (currentGame.isGameOver()) {
        b.put("winner", currentGame.getWinner().getName());
      }
      res.type("application/json");
      return GSON.toJson(b.build());
    }

  }

  /**
   * Play a turn in a given game.
   *
   * @author carlos
   */
  public static class TurnPlayer implements Route {

    private Map<String, Game> games;

    public TurnPlayer(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      /**
       * Format of this map: self -> player ID | playedCard -> the card they
       * just played |
       */
      QueryParamsMap qm = req.queryMap();

      String currGameID = req.params("id");
      Game currentGame = games.get(currGameID); // set this to the value of
                                                // the
      // game
      Map<Integer, Player> playerMap = currentGame.getPlayers();
      // Gets the player from the query params map
      Player currentPlayer = playerMap.get(Integer.parseInt(qm
          .value("playerID")));
      Card currentCard = currentPlayer.getCard(Integer.parseInt(qm
          .value("card")));
      PlayerCard curr = new PlayerCard(currentPlayer, currentCard);

      // Returns updated game state or appropriate error code
      if (currentGame.playCard(curr)) {
        res.status(OK);

        return GSON.toJson(ImmutableMap.of("game", currentGame));
      }
      res.status(NO_CONTENT);
      return "";
    }
  }

  public static class GameJoiner implements Route {

    private Map<String, Game> games;

    public GameJoiner(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      QueryParamsMap qm = req.queryMap();
      String id = req.params("id");

      Game currentGame = games.get(id);
      Player p = new HumanPlayer(qm.value("name"));

      ImmutableMap.Builder<String, Object> vars = new ImmutableMap.Builder<>();
      vars.put("playerID", currentGame.addPlayer(p));
      vars.put("name", qm.value("name"));

      if (currentGame.isReady()) {
        currentGame.startGame();
      }

      res.status(OK);
      res.type("application/json");
      return GSON.toJson(vars.build());
    }
  }

  /**
   * Handles when a player leaves a game--puts an AI player in their place.
   * 
   * @author louisa
   *
   */
  public static class LeaveGame implements Route {
    private Map<String, Game> games;

    public LeaveGame(Map<String, Game> games) {
      this.games = games;

    }

    @Override
    public Object handle(Request req, Response res) {
      String gameID = req.params("id");
      Game currentGame = games.get(gameID);

      QueryParamsMap qm = req.queryMap();
      int playerID = Integer.parseInt(qm.value("playerID"));
      Player gonePlayer = currentGame.getPlayers().get(playerID);
      AIPlayer newPlayer = currentGame.replaceWithAIPlayer(gonePlayer);

      AIThread ai = new AIThread(currentGame, newPlayer);
      ai.start();

      currentGame.getMessenger().log(gonePlayer.getName() + " left the game.");

      res.redirect("/home");
      return "";
    }

  }

  /**
   * Handles when an AI Player joins the game.
   *
   * @author louisa
   *
   */
  public static class AIPlayerJoiner implements Route {

    private Map<String, Game> games;
    private Random generator;
    private List<String> hardNamesLeft;
    private List<String> easyNamesLeft;

    public AIPlayerJoiner(Map<String, Game> games) {
      this.games = games;
      generator = new Random();
    }

    @Override
    public Object handle(Request req, Response res) {
      QueryParamsMap qm = req.queryMap();
      String gameID = req.params("id");
      Boolean isHard = Boolean.parseBoolean(qm.value("isHard"));
      AIPlayer p;

      Game currentGame = games.get(gameID);
      String playerName;
      List<String> playerNamesInGame = new ArrayList<String>();
      for (Player player : currentGame.getPlayerList()) {
        playerNamesInGame.add(player.getName());
      }

      if (isHard) {
        hardNamesLeft = new ArrayList<String>();
        for (String name : hardAIPlayers) {
          if (!playerNamesInGame.contains(name)) {
            hardNamesLeft.add(name);
          }
        }

        playerName = hardNamesLeft.get(generator.nextInt(hardNamesLeft.size()));
        if (currentGame instanceof Hearts) {
          p = new HeartsAIPlayer(playerName);
        } else if (currentGame instanceof Spades) {
          p = new SpadesAIPlayer(playerName);
        } else if (currentGame instanceof Setback) {
          p = new SetbackAIPlayer(playerName);
        } else if (currentGame instanceof GenericGame) {
          p = new GenericGameAIPlayer(playerName);
        } else {
          p = new AIPlayer(playerName);
        }
      } else {
        easyNamesLeft = new ArrayList<String>();
        for (String name : easyAIPlayers) {
          if (!playerNamesInGame.contains(name)) {
            easyNamesLeft.add(name);
          }
        }
        playerName = easyNamesLeft.get(generator.nextInt(easyNamesLeft.size()));
        easyNamesLeft.remove(playerName);
        p = new AIPlayer(playerName);
      }

      ImmutableMap.Builder<String, Object> vars = new ImmutableMap.Builder<>();
      int playerID = currentGame.addPlayer(p);

      if (playerID != -1) {
        AIThread ai = new AIThread(currentGame, p);
        ai.start();
      }
      vars.put("playerID", playerID);
      vars.put("name", p.getName());

      if (currentGame.isReady()) {
        currentGame.startGame();
      }

      res.status(OK);
      res.type("application/json");
      return GSON.toJson(vars.build());
    }
  }

  public static class GameCreator implements TemplateViewRoute {

    private Map<String, Game> games;

    public GameCreator(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public ModelAndView handle(Request req, Response res) {
      QueryParamsMap qm = req.queryMap();

      String type = qm.value("type");
      String name = qm.value("name");
      Game toCreate = null;

      if (name.equals("")) {
        name = "unnamed";
      }

      if (type != null) {
        switch (type) {
        case "hearts":
          toCreate = new Hearts("Hearts: " + name);
          break;
        case "spades":
          toCreate = new Spades("Spades: " + name);
          break;
        case "setback":
          toCreate = new Setback("Setback: " + name);
          break;
        default:
          toCreate = null;
          break;
        }
      }

      String id = String.valueOf(toCreate.hashCode());

      games.put(id, toCreate);
      res.redirect("/game/" + id);
      return null;
    }

  }

  /**
   * Handles the creation of a generic game.
   *
   * @author louisa
   *
   */
  public static class OwnGameCreator implements TemplateViewRoute {

    private Map<String, Game> games;

    public OwnGameCreator(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public ModelAndView handle(Request req, Response res) {
      QueryParamsMap qm = req.queryMap();

      boolean isMaxPoints = Boolean.parseBoolean(qm.value("maxpoints"));
      List<Integer> scoring = new ArrayList<Integer>();
      List<Integer> excludeCards = new ArrayList<Integer>();
      for (int i = 0; i < 52; i++) {
        scoring.add(Integer.parseInt(qm.value("scoringmethod" + i)));
        if (qm.value("include" + i) == null) {
          excludeCards.add(i);

        }
      }

      excludeCards.forEach(System.out::println);
      boolean endsByRound = Boolean.parseBoolean(qm.value("endsbyround"));
      int endGamePoint = Integer.parseInt(qm.value("endgamepoint"));
      String suit = qm.value("trumpsuit");
      boolean isAceHigh = Boolean.parseBoolean(qm.value("isacehigh"));
      String name = qm.value("gamename");
      boolean playingWithTrumpBroken = Boolean.parseBoolean(qm
          .value("istrumpbroken"));
      
      boolean callTricks = Boolean.parseBoolean(qm
        .value("istrickcalling"));;

      Suit trumpSuit;
      switch (suit) {
      case "spades":
        trumpSuit = Suit.SPADES;
        break;
      case "hearts":
        trumpSuit = Suit.HEARTS;
        break;
      case "clubs":
        trumpSuit = Suit.CLUBS;
        break;
      case "diamonds":
        trumpSuit = Suit.DIAMONDS;
        break;
      default:
        trumpSuit = null;
      }

      int cardsInHand = Integer.parseInt(qm.value("cardsinhand"));
      int tricksmissed = Integer.parseInt(qm.value("tricksmissed"));
      int trickscalled = Integer.parseInt(qm.value("trickscorrect"));
      int tricksover = Integer.parseInt(qm.value("tricksover"));

      Game toCreate = new GenericGame(isMaxPoints, scoring,
          endsByRound, endGamePoint, isAceHigh, cardsInHand,
          trumpSuit, callTricks, name, excludeCards,
          playingWithTrumpBroken);

      ((GenericGame) toCreate).setTricksCalled(trickscalled);
      ((GenericGame) toCreate).setTricksMissed(tricksmissed);
      ((GenericGame) toCreate).setTricksOver(tricksover);

      String id = String.valueOf(toCreate.hashCode());
      games.put(id, toCreate);
      res.redirect("/game/" + id);
      return null;
    }
  }

  public static class GameDisplayHandler implements TemplateViewRoute {

    private Map<String, Game> games;

    public GameDisplayHandler(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public ModelAndView handle(Request req, Response res) {
      Map<String, Object> variables = ImmutableMap.of("title", "Game", "id",
          req.params("id"), "rules", games.get(req.params("id")).getRules());

      return new ModelAndView(variables, "game.ftl");
    }
  }

  public static class TrickCaller implements Route {
    private Map<String, Game> games;

    public TrickCaller(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      String id = req.params("id");
      Game currentGame = games.get(id);
      QueryParamsMap qm = req.queryMap();

      // TODO error checking
      int playerID = Integer.valueOf(qm.value("playerID"));
      int tricks = Integer.valueOf(qm.value("tricks"));

      Player p = currentGame.getPlayers().get(playerID);
      if (currentGame.callTricks(p, tricks)) {
        res.status(OK);
        return "";
      } else {
        res.status(NO_CONTENT);
      }
      return "";
    }
  }

  public static class MessageGetter implements Route {
    private Map<String, Game> games;

    public MessageGetter(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      String id = req.params("id");

      Game currentGame = games.get(id);

      String timeStamp = req.queryMap().value("t");

      if (timeStamp == null) {
        timeStamp = "0";
      }

      long time = Long.valueOf(timeStamp);

      List<Message> retval = currentGame.getMessenger().since(time);
      res.type("application/json");
      return GSON.toJson(retval);
    }
  }

  public static class MessageSender implements Route {
    private Map<String, Game> games;

    public MessageSender(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public Object handle(Request req, Response res) {
      String id = req.params("id");
      Game currentGame = games.get(id);
      QueryParamsMap qm = req.queryMap();

      // TODO error checking
      int playerID = Integer.valueOf(qm.value("playerID"));
      String msg = qm.value("message");

      Message m = new Message(playerID, msg);

      currentGame.getMessenger().addMessage(m);
      res.status(NO_CONTENT);
      return "";
    }
  }

  public static class GameInfoTable implements TemplateViewRoute {
    private Map<String, Game> games;

    public GameInfoTable(Map<String, Game> games) {
      this.games = games;
    }

    @Override
    public ModelAndView handle(Request req, Response res) {

      Map<String, Object> vars = new HashMap<>();
      @SuppressWarnings("rawtypes")
      List<ImmutableMap> gList = new LinkedList<ImmutableMap>();

      @SuppressWarnings("rawtypes")
      Comparator<ImmutableMap> compare = new Comparator<ImmutableMap>() {
        public int compare(ImmutableMap o1, ImmutableMap o2) {
          if ((Integer) o1.get("players") > (Integer) o2.get("players")) {
            return 1;
          } else if ((Integer) o1.get("players") < (Integer) o2.get("players")) {
            return -1;
          } else {
            if (o1.get("kind").toString().compareTo(o2.get("kind").toString()) != 0) {
              return o1.get("kind").toString()
                  .compareTo(o2.get("kind").toString());
            } else {
              return o1.get("name").toString()
                  .compareTo(o2.get("name").toString());
            }
          }
        }
      };

      for (String id : games.keySet()) {
        Game g = games.get(id);

        String kind;
        if (g instanceof Hearts) {
          kind = "Hearts";
        } else if (g instanceof Spades) {
          kind = "Spades";
        } else if (g instanceof Setback) {
          kind = "Setback";
        } else if (g instanceof GenericGame) {
          kind = "Generic Game";
        } else {
          kind = "";
        }

        if (!g.isGameOver()) {
          gList.add(ImmutableMap.of("id", id, "name", g.getName(), "kind",
              kind, "players", g.numPlayers()));
        }
      }

      Collections.sort(gList, compare);

      vars.put("games", gList);

      return new ModelAndView(vars, "games_table.ftl");
    }
  }

  /**
   * Handles the game creation page serving.
   *
   */
  public static class CreateGamePageHandler implements TemplateViewRoute {

    @Override
    public ModelAndView handle(Request req, Response res) {
      Map<String, Object> variables = ImmutableMap.of("title", "GameGame!");
      return new ModelAndView(variables, "create.ftl");
    }
  }

  /**
   * Handles the generic game creation page serving.
   *
   */
  public static class GenericGamePageHandler implements TemplateViewRoute {

    @Override
    public ModelAndView handle(Request req, Response res) {
      Map<String, Object> variables = ImmutableMap.of("title", "GameGame!");
      return new ModelAndView(variables, "genericgame.ftl");
    }
  }
}
