package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.Deck;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.HeartsAIPlayer;
import edu.brown.cs.group.player.Player;

/**
 * An implementation of the Hearts card game.
 */
public class Hearts extends Game {

  private boolean firstHandOfRound;
  private boolean isHeartsBroken;
  private boolean queenOfSpadesWasPlayed;
  private Player firstPointWinner;

  private final int QUEEN_OF_SPADES_SCORE = 13;

  /**
   * Creates an instance of a Hearts game.
   *
   * @param players
   *          a map of playerNumner to the players. (The playerNumbers must
   *          range between 0 and 3, inclusive). This is because there can only
   *          be four player in this game.
   * @param name
   *          the name of the game.
   */
  public Hearts(Map<Integer, Player> players, String name) {
    super(players, name);
    setPot(new ArrayList<PlayerCard>());
    setCurrentOnSuit(null);
    this.queenOfSpadesWasPlayed = false;
    setReverseScoring(true);
  }

  /**
   * Creates a hearts game with no players.
   *
   */
  public Hearts() {
    this(new HashMap<Integer, Player>(), "test game");
  }

  /**
   * Constructs a Hearts game with a user-specified name.
   *
   * @param name
   *          The game creator's unique name for their game of Hearts.
   */
  public Hearts(String name) {
    this(new HashMap<Integer, Player>(), name);
  }

  /**
   * Sets the first player of the game to be the player with the two of clubs.
   *
   */
  private void setFirstPlayer() {
    for (Player player : getPlayerList()) {
      for (Card card : player.getHand()) {
        if (card.getRank().equals(Rank.TWO)
            && card.getSuit().equals(Suit.CLUBS)) {
          setWhoseTurn(player.getID());
          break;
        }
      }
    }
  }

  @Override
  protected void resetRound() {
    if (!getPot().isEmpty()) {
      setWhoseTurn(evaluatePot(getPot()).getID());
      this.getMessenger().log(
          getPlayers().get(getWhoseTurn()).getName() + " took the pot.");
      super.resetRound();
      firstHandOfRound = false;
    }
    if (!getPlayers().get(0).hasCards()) {
      if (firstPointWinner != null) {
        for (int i = 0; i < NUM_PLAYERS; i++) {
          Player player = getPlayers().get(i);
          if (player != firstPointWinner) {
            player.addToScore(26);
          } else {
            player.addToScore(-26);
          }
        }
      }
      if (!isGameOver()) {
        dealCards();
        firstHandOfRound = true;
        isHeartsBroken = false;
        setFirstPlayer();
        firstPointWinner = null;
      }
      this.stateChanged();
    }
  }

  @Override
  public boolean isGameOver() {
    for (Player player : getPlayers().values()) {
      if (player.getScore() >= 100) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean playCard(PlayerCard turn) {
    if (isValidMove(turn)) {
      if (turn.getCard().getSuit().equals(Suit.SPADES)
          && turn.getCard().getRank().equals(Rank.QUEEN)) {
        queenOfSpadesWasPlayed = true;
      }
      if (turn.getCard().getSuit().equals(Suit.HEARTS)) {
        isHeartsBroken = true;
      }
      if (getPot().isEmpty()) {
        setCurrentOnSuit(turn.getCard().getSuit());
      }
      addToPot(turn);
      turn.getPlayer().removeCard(turn.getCard());
      setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
      this.getMessenger().log(
          turn.getPlayer().getName() + " played " + turn.getCard().toString());

      if (getPot().size() == NUM_PLAYERS) {
        resetRound();
      }
      return true;
    }

    return false;
  }

  @Override
  public boolean isValidMove(PlayerCard turn) {
    if (getWhoseTurn() != turn.getPlayer().getID()) {
      return false;
    }
    for (PlayerCard currentTurn : getPot()) {
      if (currentTurn.getPlayer().equals(turn.getPlayer())) {
        return false;
      }
    }

    if (getPot().isEmpty()) {
      if (turn.getCard().getSuit().equals(Suit.HEARTS)) {
        if (isHeartsBroken) {
          return true;
        } else if (turn.getPlayer().numberOfSuit(Suit.HEARTS) == turn
            .getPlayer().handSize()) {
          return true;
        }
        return false;
      }
    }
    if (firstHandOfRound) {
      if (turn.getCard().getSuit().equals(Suit.HEARTS)
          || (turn.getCard().getSuit().equals(Suit.SPADES) && turn.getCard()
              .getRank().equals(Rank.QUEEN))) {
        return false;
      } else if (getPot().isEmpty()
          && (!turn.getCard().getSuit().equals(Suit.CLUBS) || !turn.getCard()
              .getRank().equals(Rank.TWO))) {
        return false;
      }
    }
    if (turn.getPlayer().hasSuit(getCurrentOnSuit())
        && (!turn.getCard().getSuit().equals(getCurrentOnSuit()))) {
      return false;
    }
    return true;
  }

  @Override
  public Player evaluatePot(List<PlayerCard> player) {
    PlayerCard highest = getPot().get(0);
    for (PlayerCard pc : getPot()) {
      if (pc.getCard().getSuit().equals(getCurrentOnSuit())
          && Rank.compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
        highest = pc;
      }
    }
    for (PlayerCard pc : getPot()) {
      if (pc.getCard().getSuit().equals(Suit.HEARTS)) {
        highest.getPlayer().addToScore(1);
      } else if (pc.getCard().getSuit().equals(Suit.SPADES)
          && pc.getCard().getRank().equals(Rank.QUEEN)) {
        highest.getPlayer().addToScore(QUEEN_OF_SPADES_SCORE);
      } else {
        continue;
      }
      if (firstHandOfRound) {
        firstPointWinner = highest.getPlayer();
      } else if (firstPointWinner != highest.getPlayer()) {
        firstPointWinner = null;
      }
    }
    return highest.getPlayer();
  }

  @Override
  public void dealCards() {
    Deck deck = new Deck(true, new ArrayList<Integer>());
    int currPlayer = 0;
    while (!deck.isEmpty()) {
      getPlayers().get(currPlayer).addCard(deck.draw());
      currPlayer = (currPlayer + 1) % NUM_PLAYERS;
    }
  }

  @Override
  public Player getWinner() {
    if (!isGameOver()) {
      throw new IllegalStateException("Game is not over");
    }
    Player winner = getPlayers().get(0);
    for (Player player : getPlayers().values()) {
      if (player.getScore() < winner.getScore()) {
        winner = player;
      }
    }
    return winner;
  }

  @Override
  public void startGame() {
    resetRound();
  }

  /**
   * Specifies if hearts has been broken yet.
   *
   * @return True if hearts has been broken and false otherwise
   */
  public boolean isHeartsBroken() {
    return isHeartsBroken;
  }

  /**
   * Determines if the queen of spades was played in the game of not.
   * 
   * @return true if the queen of spades was played, false otherwise.
   */
  public boolean queenOfSpadesWasPlayed() {
    return queenOfSpadesWasPlayed;
  }

  public boolean isFirstHandOfRound() {
    return firstHandOfRound;
  }

  @Override
  public AIPlayer replaceWithAIPlayer(Player p) {
    List<Card> newHand = new ArrayList<Card>();
    for (int i = 0; i < p.getHand().size(); i++) {
      Card newCard = new Card(p.getHand().get(i).getRank(), p.getHand().get(i)
          .getSuit());
      newHand.add(newCard);
    }

    AIPlayer newPlayer = new HeartsAIPlayer(p.getName(), p.getID(), newHand,
        p.getScore());
    addPlayer(newPlayer, newPlayer.getID());
    return newPlayer;
  }

  @Override
  public String getRules() {
    StringBuilder sb = new StringBuilder();
    String title = "You are currently playing Hearts.<br>";
    String goal = "GOAL: Get the lowest number of total points.<br>";
    String points = "POINT CARDS: All hearts are worth 1 point. The Queen of Spades is worth 13 points.<br>";
    String validMoves = "VALID MOVES: On your turn, you must play a card that is \"on suit\","
        + " meaning that it matches the first card played in that round. If you don't have that"
        + " suit, you can play any other card in your hand. However, if it is the first round of"
        + " a hand, you can't play any point-scoring cards (hearts or the queen of spades). Lastly,"
        + " you can't start a round with a heart unless they have already been \"broken\", meaning"
        + " that a heart was played by someone who didn't have the \"on suit\" at some point."
        + " <br><br> The player with the two of clubs must play that as their first card.<br>";
    String gamePlay = "GAMEPLAY: The player who has the highest trump card in the previous"
        + " round starts the next round; if no trump cards were played, the player who has the highest"
        + "\"on suit\" card in the previous round starts the next round. The game continues until a"
        + " player gets over 100 points, at which point the player with the least points wins.<br>";
    sb.append(title);
    sb.append(goal);
    sb.append(points);
    sb.append(validMoves);
    sb.append(gamePlay);
    return sb.toString();
  }
}
