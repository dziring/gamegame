package edu.brown.cs.group.cardGame;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import spark.ExceptionHandler;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Spark;
import spark.TemplateViewRoute;
import spark.template.freemarker.FreeMarkerEngine;

import com.google.common.collect.ImmutableMap;

import freemarker.template.Configuration;

/**
 * Runs a "GameGame" program either through a command line argument or through a
 * website.
 *
 * @author dziring
 */
public final class Main {

  /*
   * HTTP status codes
   * 
   * 304 -- Not Modified: Returned when the client makes a conditional request
   * for a resource and the server wants to indicate that it has not
   * changed--the client's copy is still correct 204 -- No Content: Returned
   * when a request was good, but there will be no content returned in the
   * response. E.g., playing your turn on a game. 403 -- Forbidden: Pretty
   * self-explanatory.
   */
  public static final int SERVER_ERROR = 500;
  public static final int NOT_MODIFIED = 304;
  public static final int NO_CONTENT = 204;
  public static final int OK = 200;

  private final Map<String, Game> games = new HashMap<String, Game>();

  /**
   * Creates a new instance of the Main class with the command line arguments.
   *
   * @param args
   *          Command line arguments
   */
  public static void main(String[] args) {
    new Main(args).run();
  }

  private String[] args;

  private Main(String[] args) {
    this.args = args;
  }

  private void run() {
    OptionParser parser = new OptionParser();

    parser.accepts("gui");
    OptionSpec<Integer> portSpec = parser.accepts("port").withRequiredArg()
        .ofType(Integer.class);

    OptionSpec<String> stringSpec = parser.nonOptions().ofType(String.class);

    OptionSet options;
    try {
      options = parser.parse(args);
    } catch (OptionException e) {
      System.out.println("ERROR: " + e.getMessage());
      return;
    }
    if (options.has("port")) {
      try {
        Spark.setPort(options.valueOf(portSpec));
      } catch (OptionException e) {

        System.out.println("ERROR: Multiple arguments given for port number.");
        return;
      }
    }
    List<String> stringArgs = stringSpec.values(options);

    // if (stringArgs.size() != COMMAND_LINE_ARGS) {
    // String usageErrorString =
    // "ERROR: Usage: " + "<sql_db> "
    // + "[--port <number>] [--gui]";
    // System.out.println(usageErrorString);
    // return;
    // }

    try {
      if (options.has("gui")) {
        // GUIController gui = new GUIController(mapsDB);
        // gui.runGui();
        runSparkServer(stringArgs);
      } else {
        // Repl.runRepl(mapsDB, mapsGraph, kdTree);
      }
    } catch (Exception e) {
      // e.printStackTrace();
      System.out.println("ERROR: " + e.getMessage());
    }
  }

  private static FreeMarkerEngine createEngine() throws IOException {
    Configuration config = new Configuration();
    File templates = new File("src/main/resources/spark/template/freemarker");
    try {
      config.setDirectoryForTemplateLoading(templates);
    } catch (IOException ioe) {
      String exceptionString = "Unable to use " + templates
          + " for template loading.%n";
      throw new IOException(exceptionString);
    }
    return new FreeMarkerEngine(config);
  }

  private void runSparkServer(List<String> stringArgs) {
    String usageErrorString = "ERROR: Usage: <startName> "
        + "<endName> <sql_db> " + "[--port <number>] [--gui]";
    // if (stringArgs.size() != 1) {
    // System.out.println(usageErrorString);
    // return;
    // }

    Spark.externalStaticFileLocation("src/main/resources/static");
    Spark.exception(Exception.class, new ExceptionPrinter());

    FreeMarkerEngine freeMarker;
    try {
      freeMarker = createEngine();
    } catch (IOException e) {
      System.out.println("ERROR: " + e.getMessage());
      return;
    }

    Spark.get("/home", new MainPageHandler(), freeMarker);
    Spark.get("/games", new GuiHandlers.GameInfoTable(games), freeMarker);

    Spark.get("/create", new GuiHandlers.CreateGamePageHandler(), freeMarker);
    Spark.post("/create", new GuiHandlers.GameCreator(games), freeMarker);
    Spark.get("/createown", new GuiHandlers.GenericGamePageHandler(),
        freeMarker);
    Spark.post("/createown", new GuiHandlers.OwnGameCreator(games), freeMarker);
    // make a handler for the page where you submit your generic game (use
    // OwnGameHandler to make it)
    Spark.get("/game/:id", new GuiHandlers.GameDisplayHandler(games),
        freeMarker);
    Spark.post("/game/:id/join", new GuiHandlers.GameJoiner(games));
    Spark.post("/game/:id/aijoin", new GuiHandlers.AIPlayerJoiner(games));
    Spark.get("/game/:id/update", new GuiHandlers.GameUpdate(games));
    Spark.post("/game/:id/turn", new GuiHandlers.TurnPlayer(games));
    Spark.post("/game/:id/calltricks", new GuiHandlers.TrickCaller(games));

    Spark.get("/game/:id/leave", new GuiHandlers.LeaveGame(games));

    Spark.get("/game/:id/messages", new GuiHandlers.MessageGetter(games));
    Spark.post("/game/:id/messages", new GuiHandlers.MessageSender(games));
  }

  /**
   * Taken from Stars code to print errors. // Spark.get();
   *
   * @author carlos
   */
  private static class ExceptionPrinter implements ExceptionHandler {
    @Override
    public void handle(Exception e, Request req, Response res) {
      res.status(SERVER_ERROR);
      StringWriter stacktrace = new StringWriter();
      try (PrintWriter pw = new PrintWriter(stacktrace)) {
        pw.println("<pre>");
        e.printStackTrace(pw);
        pw.println("</pre>");
      }
      res.body(stacktrace.toString());
      // System.err.print(stacktrace.toString());
    }
  }

  /**
   * Handles the main page serving.
   *
   * @author carlos
   */
  private class MainPageHandler implements TemplateViewRoute {

    @Override
    public ModelAndView handle(Request req, Response res) {
      Map<String, Object> variables = ImmutableMap.of("title", "GameGame!");
      return new ModelAndView(variables, "home.ftl");
    }
  }

}
