package edu.brown.cs.group.cardGame;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.player.Player;

/**
 * A class the stores a player and the card they just played in a
 * particular hand.
 *
 */
public class PlayerCard {

  private Player player;
  private Card card;

  /**
   * Creates a PlayerCard that represents a turn.
   * @param player the player who made the turn.
   * @param card the card that the player played.
   */
  public PlayerCard(Player player, Card card) {
    this.player = player;
    this.card = card;
  }

  /**
   * Gets the player associated with this turn.
   * @return the player associated with this turn.
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Gets the card associated with this turn.
   * @return the card associated with this turn.
   */
  public Card getCard() {
    return card;
  }

  @Override
  public String toString() {
    return player.toString() + ": " + card.toString();
  }
}
