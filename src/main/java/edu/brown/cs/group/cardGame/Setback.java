package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Deck;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.Player;
import edu.brown.cs.group.player.SetbackAIPlayer;

public class Setback extends Game {

  private int cardsInHand = 6;

  private Map<Player, List<PlayerCard>> cardsTaken;

  public Setback(Map<Integer, Player> players, String name) {
    super(players, name);
    cardsTaken = new HashMap<Player, List<PlayerCard>>();
    setCallsTricks(true);
  }

  /**
   * Constructs a Setback game with a user-specified name.
   *
   * @param name
   *          The game creator's unique name for their game of Setback.
   */
  public Setback(String name) {
    super(name);
    setCallsTricks(true);
  }

  public Setback() {
    super();
    setCallsTricks(true);
  }

  @Override
  public Player evaluatePot(List<PlayerCard> player) {
    PlayerCard highest = getPot().get(0);
    for (PlayerCard pc : getPot()) {
      // Played card is trump
      if (pc.getCard().getSuit().equals(super.getTrump())) {
        if (!highest.getCard().getSuit().equals(super.getTrump())) {
          highest = pc;
          continue;
        }
        // Both cards are trump
        if (Rank.compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
        // Neither card is trump
      } else if (!highest.getCard().getSuit().equals(super.getTrump())) {
        if (pc.getCard().getSuit().equals(getCurrentOnSuit())
            && Rank
                .compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
      } // else highest is trump and curr is not, so ignore
    }
    return highest.getPlayer();
  }

  /**
   * Allows to set the number of cards in each hand. FOR TEST PURPOSES ONLY.
   * 
   * @param num
   *          the number of cards in each hand.
   */
  public void setCardsInHand(int num) {
    this.cardsInHand = num;
  }

  @Override
  public boolean playCard(PlayerCard turn) {
    if (isValidMove(turn)) {
      if (getPot().isEmpty()) {
        setCurrentOnSuit(turn.getCard().getSuit());
        if (getTrump() == null) {
          setTrump(getCurrentOnSuit());
        }
      }
      addToPot(turn);
      turn.getPlayer().removeCard(turn.getCard());
      setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
      this.getMessenger().log(
          turn.getPlayer().getName() + " played " + turn.getCard().toString());
      if (getPot().size() == NUM_PLAYERS) {
        resetRound();
      }
      return true;
    }
    return false;
  }

  @Override
  protected void resetRound() {
    if (!getPot().isEmpty()) {
      setWhoseTurn(evaluatePot(getPot()).getID());
      this.getMessenger().log(
          getPlayers().get(getWhoseTurn()).getName() + " took the pot.");
      cardsTaken.get(getPlayers().get(getWhoseTurn())).addAll(getPot());
    }
    if (!getPlayers().get(0).hasCards()) {
      if (!getPot().isEmpty()) {
        updateScores();
      }
      if (!isGameOver()) {
        dealCards();
        setTrump(null);
        tricksCalled = new HashMap<Integer, Integer>();
        cardsTaken = new HashMap<Player, List<PlayerCard>>();
        for (Player player : getPlayerList()) {
          cardsTaken.put(player, new ArrayList<PlayerCard>());
        }
      }
      this.stateChanged();
    }
    if (!getPot().isEmpty()) {
      super.resetRound();
    }
  }

  private void updateScores() {
    // lowest of trump
    // highest of trump
    // jack of trump
    // game point
    Map<Player, Integer> currentRoundScores = new HashMap<Player, Integer>();
    PlayerCard lowest = null;
    PlayerCard highest = null;
    Player highestGamePoint = null;
    Map<Player, Integer> gamePointScores = new HashMap<Player, Integer>();
    for (Player player : getPlayerList()) {
      currentRoundScores.put(player, 0);
      gamePointScores.put(player, 0);
      for (PlayerCard current : cardsTaken.get(player)) {
        if (current.getCard().getSuit().equals(getTrump())) {
          if (lowest == null
              || current.getCard().getRank().getValue() < lowest.getCard()
                  .getRank().getValue()) {
            lowest = new PlayerCard(player, current.getCard());
          }
          if (highest == null
              || current.getCard().getRank().getValue() > highest.getCard()
                  .getRank().getValue()) {
            highest = new PlayerCard(player, current.getCard());
          }
          if (current.getCard().getRank().equals(Rank.JACK)) {
            currentRoundScores.put(player, 1);
          }
        }
        int value = current.getCard().getRank().getValue();
        if (value == 10) {
          gamePointScores.put(player, gamePointScores.get(player) + 10);
        } else if (value > 10) {
          gamePointScores.put(player, gamePointScores.get(player)
              + (value % 10));
        }
      }
      if (highestGamePoint == null) {
        highestGamePoint = player;
      } else if (gamePointScores.get(player) > gamePointScores
          .get(highestGamePoint)) {
        highestGamePoint = player;
      }
    }
    currentRoundScores.put(lowest.getPlayer(),
        currentRoundScores.get(lowest.getPlayer()) + 1);
    currentRoundScores.put(highest.getPlayer(),
        currentRoundScores.get(highest.getPlayer()) + 1);
    currentRoundScores.put(highestGamePoint,
        currentRoundScores.get(highestGamePoint) + 1);

    for (Player player : currentRoundScores.keySet()) {
      int called = tricksCalled.get(player.getID());
      int gotten = currentRoundScores.get(player);
      if (called > gotten) {
        player.addToScore(-called);
      } else {
        player.addToScore(gotten);
      }
    }
  }

  @Override
  public boolean callTricks(Player player, int tricks) {
    if (getWhoseTurn() != player.getID()) {
      return false;
    }
    if (tricks < 0 || tricks > 4 || tricksCalled.containsKey(player)) {
      return false;
    }
    if (tricksCalled.size() > 0) {
      int highestTricksSoFar = Collections.max(tricksCalled.values());
      if (tricks <= highestTricksSoFar && tricks != 0) {
        return false;
      }
      if (tricksCalled.size() == 3 && highestTricksSoFar == 0 && tricks == 0) {
        return false;
      }
    }
    setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
    tricksCalled.put(player.getID(), tricks);
    int playerWithMaxTricks = 0;
    if (tricksCalled.size() == NUM_PLAYERS) {
      for (int curPlayer : tricksCalled.keySet()) {
        int curTricks = tricksCalled.get(curPlayer);
        if (curTricks > playerWithMaxTricks) {
          playerWithMaxTricks = curPlayer;
        }
      }
      for (int curPlayer : tricksCalled.keySet()) {
        if (curPlayer != playerWithMaxTricks) {
          tricksCalled.put(curPlayer, 0);
        }
      }
      setWhoseTurn(playerWithMaxTricks);
    }
    return true;
  }

  @Override
  public boolean isValidMove(PlayerCard turn) {
    if (tricksCalled.size() < NUM_PLAYERS) {
      return false;
    }
    if (getWhoseTurn() != turn.getPlayer().getID()) {
      return false;
    }
    for (PlayerCard currentTurn : getPot()) {
      if (currentTurn.getPlayer().equals(turn.getPlayer())) {
        return false;
      }
    }
    if (!getPot().isEmpty()) {

      if (turn.getPlayer().hasSuit(getCurrentOnSuit())
          && (!turn.getCard().getSuit().equals(getCurrentOnSuit()))
          && (!turn.getCard().getSuit().equals(super.getTrump()))) {
        return false;
      }
    }
    return true;
  }

  @Override
  public Player getWinner() {
    if (!isGameOver()) {
      throw new IllegalStateException("Game is not over");
    }
    Player winner = getPlayers().get(0);
    for (Player player : getPlayers().values()) {
      if (player.getScore() > winner.getScore()) {
        winner = player; // TODO Deal with two-way tie?
      }
    }
    return winner;
  }

  @Override
  public boolean isGameOver() {
    for (Player player : getPlayers().values()) {
      if (player.getScore() >= 10) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void startGame() {
    setPot(new ArrayList<PlayerCard>());
    resetRound();
  }

  @Override
  public void dealCards() {
    Deck deck = new Deck(true, new ArrayList<Integer>());
    int currPlayer = 0;
    int dealt = 0;
    while (Math.floor(dealt / NUM_PLAYERS) < cardsInHand) {
      getPlayers().get(currPlayer).addCard(deck.draw());
      currPlayer = (currPlayer + 1) % NUM_PLAYERS;
      dealt++;
    }
  }

  @Override
  public AIPlayer replaceWithAIPlayer(Player p) {
    List<Card> newHand = new ArrayList<Card>();
    for (int i = 0; i < p.getHand().size(); i++) {
      Card newCard = new Card(p.getHand().get(i).getRank(), p.getHand().get(i)
          .getSuit());
      newHand.add(newCard);
    }

    AIPlayer newPlayer = new SetbackAIPlayer(p.getName(), p.getID(), newHand,
        p.getScore());
    addPlayer(newPlayer, newPlayer.getID());
    return newPlayer;
  }

  @Override
  public String getRules() {
    StringBuilder sb = new StringBuilder();
    String title = "You are currently playing Setback.<br>";
    String goal = "GOAL: Get 10 or more points first.<br> In each hand, you are trying to gain 4 possible points."
        + "These points come from High, Low, Jack, and Most Game Points and are each worth 1 point. (Described in game play). <br>";
    String points = "POINT CARDS: For game points, the 10 card is worth 10 points. Jacks are worth"
        + "1 point, Queens are worth 2 points, Kings are worth 3 points, and Aces are worth 4 points. <br>";
    String validMoves = "VALID MOVES: On your turn, you must play a card that is \"on suit\","
        + " meaning that it matches the first card played in that round. If you don't have that"
        + " suit, you can play any other card in your hand. You can also play a trump suit card at any time. <br> ";
    String gamePlay = "GAMEPLAY: The game starts with bidding for trump suit, proceeds with the play of all six tricks, "
        + "and then tallying of the four possible points and scoring at the end. <br>Points to be Won: <br>"
        + "There are four points to be won in each setback hand. <br> High: the highest card in the trump suit (this might "
        + "not be the ace: you're not playing with all the cards!) <br> Low:the lowest card in the trump suit (likewise, "
        + "this may not be the two) <br> Jack:the Jack of the trump suit (may or may not even be in play) <br> Game (or Most): The sum "
        + "of \"game points\" contributed from all point cards (10, Jack, Queen, King, Ace) that are in play. "
        + "For the calculation of who has the most game points, all suits are equal, unlike the other three points. "
        + "If more than one player holds the highest number of points, nobody gets the point. <br>"
        + "For EACH trick, the person who wins each trick is defined as:<br>"
        + "1) Played the highest trump <br>"
        + "2) If no trumps were played, played the highest card in the suit that was led <br>"
        + "The first player in each round is determined in a clockwise order (based on the "
        + "previous round). The game continues until a player gets 10 or more total points (High, Low, Jack, Game), at "
        + "which point the player with these 10 or more points wins.<br>";
    sb.append(title);
    sb.append(goal);
    sb.append(points);
    sb.append(validMoves);
    sb.append(gamePlay);
    return sb.toString();
  }
}
