package edu.brown.cs.group.cardGame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.Deck;
import edu.brown.cs.group.player.AIPlayer;
import edu.brown.cs.group.player.Player;
import edu.brown.cs.group.player.SpadesAIPlayer;

public class Spades extends Game {

  private int team1score, team2score;
  private Map<Player, Integer> currentRoundScores;

  public Spades(Map<Integer, Player> players, String name) {
    super(players, name);
    setCallsTricks(true);
    setHasTeams(true);
    currentRoundScores = new HashMap<Player, Integer>();
  }

  public Spades() {
    super();
    setCallsTricks(true);
    setHasTeams(true);
    currentRoundScores = new HashMap<Player, Integer>();
  }

  /**
   * Constructs a Spades game with a user-specified name.
   *
   * @param name
   *          The game creator's unique name for their game of Spades.
   */
  public Spades(String name) {
    super(name);
    setCallsTricks(true);
    setHasTeams(true);
    currentRoundScores = new HashMap<Player, Integer>();
  }

  public Map<Integer, Integer> getTricksCalled() {
    return ImmutableMap.copyOf(tricksCalled);
  }

  protected void resetRound() {

    if (!getPot().isEmpty()) {
      Player winner = evaluatePot(getPot());
      setCurrentOnSuit(null);
      setWhoseTurn(winner.getID());
      this.getMessenger().log(
          getPlayers().get(getWhoseTurn()).getName() + " took the pot.");
      super.resetRound();
      currentRoundScores.put(winner, currentRoundScores.get(winner) + 1);
    }

    if (!getPlayers().get(0).hasCards()) {
      updateScores(currentRoundScores);
      if (!isGameOver()) {
        dealCards();
        tricksCalled = new HashMap<Integer, Integer>();
        currentRoundScores = new HashMap<Player, Integer>();
        for (Player player : getPlayerList()) {
          currentRoundScores.put(player, 0);
        }
        setPot(new ArrayList<PlayerCard>());
      }
    }
    this.stateChanged();
  }

  private void updateScores(Map<Player, Integer> roundScores) {

    for (Player player : roundScores.keySet()) {
      int called = tricksCalled.get(player.getID());
      int gotten = roundScores.get(player);
      if (called > gotten) {
        player.addToScore(-called * 10);
      } else {
        int currMod = player.getScore() % 10;
        int tricksOver = gotten - called;
        player.addToScore(called * 10 + tricksOver);
        if (currMod + tricksOver >= 10) {
          player.addToScore(-100);
        }
      }
    }
    team1score = getPlayers().get(0).getScore()
        + getPlayers().get(2).getScore();
    team2score = getPlayers().get(1).getScore()
        + getPlayers().get(3).getScore();
  }

  @Override
  public void dealCards() {
    Deck deck = new Deck(true, new ArrayList<Integer>());
    int currPlayer = 0;
    while (!deck.isEmpty()) {
      getPlayers().get(currPlayer).addCard(deck.draw());
      currPlayer = (currPlayer + 1) % NUM_PLAYERS;
    }
  }

  @Override
  public Player evaluatePot(List<PlayerCard> player) {
    PlayerCard highest = getPot().get(0);
    for (PlayerCard pc : getPot()) {
      // Played card is trump
      if (pc.getCard().getSuit().equals(Suit.SPADES)) {
        if (!highest.getCard().getSuit().equals(Suit.SPADES)) {
          highest = pc;
          continue;
        }
        // Both cards are trump
        if (Rank.compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
        // Neither card is trump
      } else if (!highest.getCard().getSuit().equals(Suit.SPADES)) {
        if (pc.getCard().getSuit().equals(getCurrentOnSuit())
            && Rank
                .compare(pc.getCard().getRank(), highest.getCard().getRank()) > 0) {
          highest = pc;
        }
      } // else highest is trump and curr is not, so ignore
    }
    return highest.getPlayer();
  }

  @Override
  public boolean playCard(PlayerCard turn) {
    if (isValidMove(turn)) {
      if (getPot().isEmpty()) {
        setCurrentOnSuit(turn.getCard().getSuit());
      }
      addToPot(turn);
      turn.getPlayer().removeCard(turn.getCard());
      setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
      this.getMessenger().log(
          turn.getPlayer().getName() + " played " + turn.getCard().toString());
      if (getPot().size() == NUM_PLAYERS) {
        resetRound();
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean callTricks(Player player, int tricks) {
    if (getWhoseTurn() != player.getID()) {
      return false;
    }
    if (tricks < 0 || tricks > 13 || tricksCalled.containsKey(player)) {
      return false;
    }
    tricksCalled.put(player.getID(), tricks);
    setWhoseTurn((getWhoseTurn() + 1) % NUM_PLAYERS);
    return true;
  }

  @Override
  public boolean isValidMove(PlayerCard turn) {
    if (tricksCalled.size() < NUM_PLAYERS) {
      return false;
    }
    if (getWhoseTurn() != turn.getPlayer().getID()) {
      return false;
    }
    for (PlayerCard currentTurn : getPot()) {
      if (currentTurn.getPlayer().equals(turn.getPlayer())) {
        return false;
      }
    }
    if (!getPot().isEmpty()) {
      if (turn.getPlayer().hasSuit(getCurrentOnSuit())
          && (!turn.getCard().getSuit().equals(getCurrentOnSuit()))) {
        return false;
      }
    }
    return true;
  }

  // Returns only one of the two players on the winning team
  @Override
  public Player getWinner() {
    if (!isGameOver()) {
      throw new IllegalStateException("Game is not over");
    }
    return team1score >= 500 ? getPlayers().get(0) : getPlayers().get(1);
  }

  @Override
  public boolean isGameOver() {
    if (team1score >= 500 || team2score >= 500) {
      return true;
    }
    return false;
  }

  @Override
  public List<Player> getPlayerList() {
    List<Player> playerList = new ArrayList<Player>();
    for (int num : getPlayers().keySet()) {
      playerList.add(getPlayers().get(num));
    }
    return playerList;
  }

  @Override
  public void startGame() {
    setPot(new ArrayList<PlayerCard>());
    team1score = 0;
    team2score = 0;
    resetRound();
  }

  @Override
  public AIPlayer replaceWithAIPlayer(Player p) {
    List<Card> newHand = new ArrayList<Card>();
    for (int i = 0; i < p.getHand().size(); i++) {
      Card newCard = new Card(p.getHand().get(i).getRank(), p.getHand().get(i)
          .getSuit());
      newHand.add(newCard);
    }

    AIPlayer newPlayer = new SpadesAIPlayer(p.getName(), p.getID(), newHand,
        p.getScore());
    addPlayer(newPlayer, newPlayer.getID());
    return newPlayer;
  }

  @Override
  public String getRules() {
    StringBuilder sb = new StringBuilder();
    String title = "You are currently playing Spades.<br>";
    String goal = "GOAL: Get 500 or more points as a team.<br> You are on a team with the person across from you. <br>";
    String points = "POINT CARDS: There are no cards in this game that individually score you points. Instead,"
        + " you earn points by winning \"tricks\" and correctly calling how many you will win beforehand. <br>";
    String validMoves = "VALID MOVES: On your turn, you must play a card that is \"on suit\","
        + " meaning that it matches the first card played in that round. If you don't have that"
        + " suit, you can play any other card in your hand. <br> ";
    String gamePlay = "GAMEPLAY: The game starts with bidding, during which each player calls how many"
        + " tricks s/he expects to win. The game proceeds with the play of all thirteen tricks. "
        + "For EACH trick, the person who wins each trick is defined as:<br>"
        + "1) Played the highest card of Spades <br>"
        + "2) If no Spades were played, played the highest card in the suit that was led <br>"
        + "The player who takes the previous trick starts the next one.<br><br>"
        + "After all thirteen tricks in the hand have been played, if you did not get "
        + "at least as many tricks as you called, you lose (tricksCalled)*10 points. "
        + "Otherwise, you score (tricksCalled)*10 + (tricksTaken-tricksCalled) points. "
        + "If you at some point (over time) call 10 tricks more than you took, you lose "
        + "100 points. <br>"
        + "The game continues until one team has at least 500 points.";
    sb.append(title);
    sb.append(goal);
    sb.append(points);
    sb.append(validMoves);
    sb.append(gamePlay);
    return sb.toString();
  }
}