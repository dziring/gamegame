package edu.brown.cs.group.games;

/**
 * A class that represents a card.
 */
public class Card {

  public static final int DIAMONDS_OFFSET = 0, CLUBS_OFFSET = 13,
  HEARTS_OFFSET = 26, SPADES_OFFSET = 39;
  private static final int ACE_INDEX = 12;

  /**
   * A Rank enum that stores the card number and its value.
   */
  public static enum Rank {
    TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(
        9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);

    private int value;

    Rank(int value) {
      this.value = value;
    }

    /**
     * Sets the value of the card.
     *
     * @param newValue The new integer value of the card
     */
    public void setValue(int newValue) {
      this.value = newValue;
    }

    /**
     * Compares the ranks of two cards by their values.
     *
     * @param o1 The first rank to compare
     * @param o2 The second rank to compare
     * @return positive if o1 greater than o2,
     * negative if o1 less then o2, else 0
     */
    public static int compare(Rank o1, Rank o2) {
      return o1.value - o2.value;
    }

    /**
     * Gets the integer value of the card.
     * @return The value of the card
     */
    public int getValue() {
      return value;
    }
  };

  /**
   * A suit enum that stores the type of card it is (spades, hearts, etc.).
   */
  public static enum Suit {
    SPADES, HEARTS, CLUBS, DIAMONDS
  };

  private Rank rank;
  private Suit suit;

  /**
   * Creates a card.
   * @param rank the rank of the card.
   * @param suit the suit of the card.
   */
  public Card(Rank rank, Suit suit) {
    this.rank = rank;
    this.suit = suit;

  }

  /**
   * If the card is a red card (Diamonds or hearts).
   * @return true if the card is red.
   */
  public boolean isRed() {
    if (suit.equals(Suit.SPADES) || suit.equals(Suit.CLUBS)) {
      return false;
    }
    return true;
  }

  /**
   * IF the card is a black card (Spades or Clubs).
   * @return true if the card is black.
   */
  public boolean isBlack() {
    return !isRed();
  }

  /**
   * Gets the rank of this card.
   * @return the rank of this card.
   */
  public Rank getRank() {
    return rank;
  }

  /**
   * Gets the suit of this card.
   * @return the suit of this card.
   */
  public Suit getSuit() {
    return suit;
  }

  /**
   * Gets the scoring index of the card to figure out easy access for
   * points.
   * @param card the card whose index we are trying to find.
   * @return the integer that represents which spot in the array the card
   *         corresponds to.
   */
  public static int getScoringIndex(Card card) {
    int index;
    if (card.getRank().equals(Rank.ACE)) {
      index = ACE_INDEX;
    } else {
      index = card.getRank().getValue() - 2;
    }

    switch (card.getSuit()) {
      case DIAMONDS:
        index += DIAMONDS_OFFSET;
        break;
      case CLUBS:
        index += CLUBS_OFFSET;
        break;
      case HEARTS:
        index += HEARTS_OFFSET;
        break;
      default:
        index += SPADES_OFFSET;
        break;
    }
    return index;
  }
  
  /**
   * Gets the the card from its scoring index value.
   *
   * @param index the index of the card
   * @return a copy of the card that index is associated with
   */
  public static Card getCardFromIndex(int index) {
    Suit suit = null;
    Rank rank = null;
    
    if (index < CLUBS_OFFSET) {
      suit = Suit.DIAMONDS;
    } else if (index < HEARTS_OFFSET) {
      suit = Suit.CLUBS;
    } else if (index < SPADES_OFFSET) {
      suit = Suit.HEARTS;
    } else {
      suit = Suit.SPADES;
    }
    
    index %= CLUBS_OFFSET;
    for (Rank currRank: Rank.values()) {
      if (index == currRank.getValue() - 2) {
        rank = currRank;
        break;
      }
    }

    return new Card(rank, suit);
  }

  @Override
  public String toString() {
    return this.rank.toString() + " of " + this.suit.toString();
  }
}
