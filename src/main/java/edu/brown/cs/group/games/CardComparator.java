package edu.brown.cs.group.games;

import java.util.Comparator;

/**
 * This class compares cards by their index to sort them.
 *
 * @author DanZiring
 *
 */
public class CardComparator implements Comparator<Card> {

  @Override
  public int compare(Card o1, Card o2) {
    if (Card.getScoringIndex((Card) o1) < Card
        .getScoringIndex((Card) o2)) {
      return -1;
    }
    if (Card.getScoringIndex((Card) o1) > Card
        .getScoringIndex((Card) o2)) {
      return 1;
    }
    return 0;
  }

}
