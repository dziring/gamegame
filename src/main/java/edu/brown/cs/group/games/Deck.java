package edu.brown.cs.group.games;

import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Class Deck that represents a deck of cards.
 */
public class Deck {
  /** Stores all of the cards in the deck. **/
  private LinkedList<Card> deck;
  private boolean isAceHigh;
  private List<Integer> excludeCards;

  /**
   * Creates a deck based on the enumerated types in the card class.
   * @param isAceHigh boolean that represents if the ace is high.
   * @param excludeCards the cards to exclude (integers represent index).
   */
  public Deck(boolean isAceHigh, List<Integer> excludeCards) {
    this.isAceHigh = isAceHigh;
    this.excludeCards = excludeCards;
    reset();
  }

  /**
   * Recreates a full deck, newly shuffled.
   */
  public void reset() {
    deck = new LinkedList<Card>();
    for (Card.Suit s : Card.Suit.values()) {
      for (Card.Rank r : Card.Rank.values()) {
        if (!isAceHigh) {
          if (r.equals(Card.Rank.ACE)) {
            r.setValue(1);
          }
        }
        Card c = new Card(r, s);
        if (!excludeCards.contains(Card.getScoringIndex(c))) {
          deck.add(c);
        }
      }
    }
    shuffle();
  }

  /**
   * Draws a card from the top of the deck.
   * @return The first card in the linked list of cards.
   */
  public Card draw() {
    return deck.pop();
  }

  /**
   * Shuffles the cards currently in the deck.
   */
  public void shuffle() {
    LinkedList<Card> newDeck = new LinkedList<Card>();
    while (deck.size() > 0) {
      int rand = (int) (Math.random() * deck.size());
      newDeck.add(deck.remove(rand));
    }
    deck = newDeck;
  }

  /**
   * Determines whether the size of the deck is empty.
   * @return true if the deck is empty.
   */
  public boolean isEmpty() {
    return deck.size() == 0;
  }

  public ImmutableList<Card> getDeckList() {
    return ImmutableList.copyOf(deck);
  }

  protected void setDeckList(LinkedList<Card> deckList) {
    deck = deckList;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    } else if (!(o instanceof Deck)) {
      return false;
    } else {
      Deck otherDeck = (Deck) o;
      return ImmutableList.copyOf(deck).equals(
          otherDeck.getDeckList());
    }
  }

  // Since we overrode the equals method, we need a new hashcode.
  @Override
  public int hashCode() {
    return 0;
  }

  /**
   * Creates a deep copy of the deck.
   *
   * @return A new deck with all of the cards the same
   */
  public Deck copy() {
    Deck copy = new Deck(isAceHigh, excludeCards);
    LinkedList<Card> deckCopy = new LinkedList<Card>();
    for (int i = 0; i < deck.size(); i++) {
      deckCopy.add(deck.get(i));
    }
    copy.setDeckList(deckCopy);
    return copy;
  }
}
