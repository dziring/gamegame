package edu.brown.cs.group.messages;

/**
 * A class for a message object.
 *
 * @author lconwill and crotger
 *
 */
public class Message implements Comparable {
  private int senderID;
  private String message;
  private long timeStamp;

  /**
   * Constructs a message from the ID of the player who sent the message and
   * their message. Adds a timestamp for when the message was sent.
   * 
   * @param senderID
   *          The player ID of the sender of the message
   * @param message
   *          The message the player sent
   */
  public Message(int senderID, String message) {
    this.senderID = senderID;
    this.message = message;
    this.timeStamp = System.currentTimeMillis();
  }

  /**
   * Gets the player ID of the sender of the message.
   * 
   * @return this.senderID
   */
  public int getSenderID() {
    return senderID;
  }

  /**
   * Returns the message contained in this message object.
   *
   * @return this.message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Returns the time stamp of the message.
   *
   * @return this.timeStamp
   */
  public long getTimeStamp() {
    return timeStamp;
  }

  @Override
  public int compareTo(Object o) {
    Message otherMessage = (Message) o;
    long otherTime = otherMessage.getTimeStamp();

    if (this.timeStamp < otherTime) {
      return -1;
    } else if (this.timeStamp > otherTime) {
      return 1;
    }

    return 0;
  }

}
