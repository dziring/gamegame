package edu.brown.cs.group.messages;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class Messenger {
  
  private List<Message> msgs;
  
  public Messenger() {
    msgs = new ArrayList<Message>();
  }
  
  public void addMessage(Message m) {
    msgs.add(m);
  }
  
  public void log(String s) {
    msgs.add(new Message(-1, s));
  }
  
  public List<Message> since(long time) {
    if(msgs.size() == 0 || msgs.get(msgs.size() - 1).getTimeStamp() <= time) {
      return Collections.emptyList();
    }
    
    int index = msgs.size() - 1;
    
    while(index >= 0 && msgs.get(index).getTimeStamp() > time) {
      index--;
    }
    
    index++;
    
    return msgs.subList(index > 0 ? index : 0, msgs.size());
  }

}
