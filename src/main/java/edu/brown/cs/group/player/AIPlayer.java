package edu.brown.cs.group.player;

import java.util.List;
import java.util.Random;

import edu.brown.cs.group.cardGame.Game;
import edu.brown.cs.group.cardGame.Setback;
import edu.brown.cs.group.games.Card;

/**
 * A class that represents a human player.
 */
public class AIPlayer extends Player {
  private Random generator;

  /**
   * Constructs a human player.
   *
   * @param name
   *          the name of the player.
   */
  public AIPlayer(String name) {
    super(name);
    generator = new Random();
  }

  /**
   * Constructs an AI player with all the necessary parts. (Used to make an AI
   * player when a human player drops out of a game).
   *
   * @param name
   *          The name of the player
   * @param id
   *          The id of the player
   * @param hand
   *          The player's hand
   * @param score
   *          The player's current score
   */
  public AIPlayer(String name, int id, List<Card> hand, int score) {
    super(name, id, hand, score);
  }

  /**
   * Generates a random move for the AIPlayer.
   *
   * @param currentGame
   *          A reference to the object for the current game being played
   * @return The card that is the AIPlayer's next move.
   */
  public Card generateMove(Game currentGame) {
    int size = super.handSize();
    return getHand().get(generator.nextInt(size));
  }

  /**
   * Makes a random bet for games that involve betting.
   *
   * @param currentGame
   *          The current game being played
   * @return A random bet for the game
   */
  public int makeBet(Game currentGame) {
    if (currentGame instanceof Setback) {
      Setback currentSetbackGame = (Setback) currentGame;
      if (currentSetbackGame.getTricksCalled().size() == 0) {
        return generator.nextInt(4);
      } else if (currentSetbackGame.getTricksCalled().containsValue(4)) {
        return 0;
      } else if (currentSetbackGame.getTricksCalled().containsValue(3)) {
        return 0;
      } else if (currentSetbackGame.getTricksCalled().containsValue(2)) {
        return generator.nextInt(2) + 3;
      } else if (currentSetbackGame.getTricksCalled().containsValue(1)) {
        return generator.nextInt(3) + 2;
      } else {
        return generator.nextInt(4);
      }

    }

    return generator.nextInt(13);

  }

  @Override
  public boolean isAI() {
    return true;
  }

  protected int generateNextInt(int size) {
    return generator.nextInt(size);
  }
}
