package edu.brown.cs.group.player;

import java.util.List;

import edu.brown.cs.group.cardGame.Game;
import edu.brown.cs.group.cardGame.GenericGame;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.CardComparator;

public class GenericGameAIPlayer extends AIPlayer {
  private CardComparator compare;

  /**
   * Constructs a setback player with a name.
   * 
   * @param name
   *          the name of the player.m
   */
  public GenericGameAIPlayer(String name) {
    super(name);
    this.compare = new CardComparator();
  }

  public GenericGameAIPlayer(String name, int id, List<Card> hand, int score) {
    super(name, id, hand, score);
    this.compare = new CardComparator();
  }

  @Override
  public Card generateMove(Game currentGame) {
    // TODO
    GenericGame currentGenericGame = (GenericGame) currentGame;
    if (currentGenericGame.getIsMaxPoints()) {
      return getHand().get(
          playALowCard(currentGenericGame.getCurrentOnSuit(),
              currentGenericGame.getTrump()));
    }

    return getHand().get(
        playAHighCard(currentGenericGame.getCurrentOnSuit(),
            currentGenericGame.getTrump()));

  }

  public int playALowCard(Suit onSuit, Suit trumpSuit) {
    int currentBest = 0;
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == onSuit) {

        if (getHand().get(currentBest).getSuit() != onSuit) {
          currentBest = i;
        } else {
          int compareVal = compare.compare(getHand().get(currentBest),
              getHand().get(i));
          if (compareVal == 1) {
            currentBest = i;
          }
        }

      } else {
        if (getHand().get(currentBest).getSuit() == trumpSuit) {
          if (getHand().get(i).getSuit() == trumpSuit) {
            int compareVal = compare.compare(getHand().get(currentBest),
                getHand().get(i));
            if (compareVal == 1) {
              currentBest = i;
            }
          } else {
            currentBest = i;
          }
        }

      }

    }

    return currentBest;

  }

  public int playAHighCard(Suit onSuit, Suit trumpSuit) {
    int currentBest = 0;
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == onSuit) {

        if (getHand().get(currentBest).getSuit() != onSuit) {
          currentBest = i;
        } else {
          int compareVal = compare.compare(getHand().get(currentBest),
              getHand().get(i));
          if (compareVal == -1) {
            currentBest = i;
          }
        }

      } else {
        if (getHand().get(currentBest).getSuit() == trumpSuit) {
          if (getHand().get(i).getSuit() == trumpSuit) {
            int compareVal = compare.compare(getHand().get(currentBest),
                getHand().get(i));
            if (compareVal == -1) {
              currentBest = i;
            }
          } else {
            currentBest = i;
          }
        }

      }

    }

    return currentBest;

  }

  @Override
  public int makeBet(Game currentGame) {
    return 2;

  }

}
