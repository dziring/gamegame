package edu.brown.cs.group.player;

import java.util.ArrayList;
import java.util.List;

import edu.brown.cs.group.cardGame.Game;
import edu.brown.cs.group.cardGame.Hearts;
import edu.brown.cs.group.cardGame.PlayerCard;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.CardComparator;

/**
 * A class that represents a setback player.
 *
 */
public class HeartsAIPlayer extends AIPlayer {
  private CardComparator compare;

  /**
   * Constructs a hear AI Player.
   * 
   * @param name
   *          the name of the player.
   */
  public HeartsAIPlayer(String name) {
    super(name);
    this.compare = new CardComparator();
  }

  public HeartsAIPlayer(String name, int id, List<Card> hand, int score) {
    super(name, id, hand, score);
    this.compare = new CardComparator();
  }

  @Override
  public Card generateMove(Game currentGame) {
    Hearts currentHeartsGame = (Hearts) currentGame;

    Suit onSuit = currentHeartsGame.getCurrentOnSuit();

    if (onSuit == null && currentHeartsGame.isFirstHandOfRound()) {
      return getHand().get(twoOfClubsIndex());
    }

    if (onSuit == null) {
      if (currentHeartsGame.isHeartsBroken()) {
        // pick a low card
        return getHand().get(pickALowCard());
      } else {
        // pick a high card of any non-hearts suit
        return getHand().get(pickAHighCard());
      }

    }

    List<Card> valid = validMoves(onSuit);

    boolean isOnSuit = false;
    if (valid.get(0).getSuit() == onSuit) {
      isOnSuit = true;
    }

    if (isOnSuit) {
      if (!currentHeartsGame.isHeartsBroken()) {
        // Be careful if:
        // it's spades--don't play anything queen or bigger
        // unless the queen of spades has been played or you have the queen
        // of
        // spades
        // or unless you're the fourth player in a round of spades and the
        // queen
        // of spades isn't in the pot
        if (onSuit.equals(Suit.SPADES)
            && (!currentHeartsGame.queenOfSpadesWasPlayed()
                || handContainsQueenOfSpades() || (currentHeartsGame.getPot()
                .size() == (Game.NUM_PLAYERS - 1) && potContainsQueenOfSpades(currentGame
                .getPot())))) {
          return valid.get(pickSpadesSmallerThanQueen(valid));
        }

        // play biggest card of that suit that's not the queen of spades
        return valid.get(pickBiggestOfSuit(onSuit, valid));

      } else {
        // play smallest card of that suit
        return valid.get(pickSmallestOfSuit(onSuit, valid));
      }

    } else {
      // if you have the queen of spades, play it (if it's not the first
      // round)!!!!
      if (indexOfQueenOfSpades() != -1
          && !currentHeartsGame.isFirstHandOfRound()) {
        return getHand().get(indexOfQueenOfSpades());
      }
      // play your biggest hearts card
      if (pickBiggestOfHearts() != -1) {
        return getHand().get(pickBiggestOfHearts());
      }
      // play any other card
      return getHand().get(0);

    }

  }

  /**
   * Finds the biggest hearts card in the hand.
   *
   * @return The index of the biggest hearts card in this.hand
   */
  private int pickBiggestOfHearts() {
    int max = 0;

    for (int i = 0; i < getHand().size(); i++) {
      Card c = getHand().get(i);
      if (c.getSuit() == Suit.HEARTS) {
        int compareVal = compare.compare(c, getHand().get(max));
        if (compareVal == 1) {
          max = i;
        }
      }
    }

    if (getHand().get(max).getSuit() == Suit.HEARTS) {
      return max;
    }

    return -1;
  }

  /**
   * Picks the largest spades smaller than the queen of spades in the valid
   * cards that can be played.
   *
   * @param valid
   *          The list of valid Cards for this turn
   * @return The index of the largest spades card smaller than the queen of
   *         spades in the hand
   */
  private int pickSpadesSmallerThanQueen(List<Card> valid) {
    int max = 0;
    for (int i = 0; i < valid.size(); i++) {
      Card c = valid.get(i);
      if (c.getRank() != Rank.QUEEN && c.getRank() != Rank.KING
          && c.getRank() != Rank.ACE) {
        int compareVal = compare.compare(getHand().get(i), getHand().get(max));
        if (compareVal == 1) {
          max = i;
        }

      }
    }

    return max;
  }

  /**
   * Finds the index of the queen of spades in the list representing the hand of
   * cards.
   *
   * @return The index of the queen of spades or -1 if the queen of spades isn't
   *         in the hand
   */
  private int indexOfQueenOfSpades() {

    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == Suit.SPADES
          && getHand().get(i).getRank() == Rank.QUEEN) {
        return i;
      }
    }

    return -1;

  }

  /**
   * Returns if the queen of spades is in this.hand.
   *
   * @return True if the hand contains the queen of spades and false otherwise
   */
  private boolean handContainsQueenOfSpades() {
    for (Card c : getHand()) {
      if (c.getSuit() == Suit.SPADES && c.getRank() == Rank.QUEEN) {
        return true;
      }
    }

    return false;
  }

  /**
   * Returns if the game's pot contains the queen of spades.
   *
   * @param pot
   *          The current pot in the game
   * @return true if the pot contains the queen of spades and false otherwise
   */
  private boolean potContainsQueenOfSpades(List<PlayerCard> pot) {
    for (PlayerCard pc : pot) {
      if (pc.getCard().getSuit() == Suit.SPADES
          && pc.getCard().getRank() == Rank.QUEEN) {
        return true;
      }
    }

    return false;

  }

  /**
   * Finds the smallest card in the hand.
   *
   * @return The smallest ranked card in the hand
   */
  private int pickALowCard() {
    int min = 0;
    for (int i = 0; i < getHand().size(); i++) {
      int compareVal = compare.compare(getHand().get(i), getHand().get(min));
      if (compareVal == -1) {
        min = i;
      }
    }

    return min;
  }

  /**
   * Picks the smallest card of the onSuit in the hand.
   *
   * @param onSuit
   *          The current onSuit
   * @param validMoves
   *          The list of valid moves for this turn
   * @return The index of the smallest ranked card of the onSuit
   */
  private int pickSmallestOfSuit(Suit onSuit, List<Card> validMoves) {
    int min = 0;
    for (int i = 0; i < validMoves.size(); i++) {
      int compareVal = compare.compare(validMoves.get(i), validMoves.get(min));
      if (compareVal == -1) {
        min = i;
      }

    }

    return min;

  }

  /**
   * Picks the biggest card of the onSuit in the hand that is *not* the queen of
   * spades.
   *
   * @param onSuit
   *          The current onSuit
   * @param validMoves
   *          The list of valid moves for this turn
   * @return The index of the largest ranked card of the onSuit
   */
  private int pickBiggestOfSuit(Suit onSuit, List<Card> validMoves) {
    int max = 0;

    for (int i = 0; i < validMoves.size(); i++) {
      Card c = validMoves.get(i);
      if (!(c.getSuit() == Suit.SPADES && c.getRank() == Rank.QUEEN)) {
        int compareVal = compare.compare(c, validMoves.get(max));
        if (compareVal == 1) {
          max = i;
        }
      }
    }

    return max;

  }

  /**
   * Finds the biggest card in the hand.
   *
   * @return The largest ranked card in the hand
   */
  private int pickAHighCard() {
    int max = 0;
    for (int i = 0; i < getHand().size(); i++) {
      Card c = getHand().get(i);
      if (c.getSuit() != Card.Suit.HEARTS) {
        if (c.getSuit() == Card.Suit.SPADES && c.getRank() == Card.Rank.QUEEN) {
          continue;
        }
        int compareVal = compare.compare(c, getHand().get(max));
        if (compareVal == 1) {
          max = i;
        }

      }

    }

    return max;

  }

  /**
   * Returns a list of all the valid cards that can be played in this turn.
   *
   * @param onSuit
   *          The onSuit of this round (null if the AI player is the first
   *          player of the round)
   * @return A list of all valid moves for this turn
   */
  private List<Card> validMoves(Suit onSuit) {
    if (onSuit == null) {
      return getHand();
    }

    List<Card> validCards = new ArrayList<Card>();
    for (Card c : getHand()) {
      if (c.getSuit() == onSuit) {
        validCards.add(c);
      }
    }

    if (validCards.size() == 0) {
      return getHand();
    }

    return validCards;

  }

  /**
   * Returns the index of the two of clubs in the player's hand
   *
   * @return The index of the two of clubs in the player's hand or -1 if it's
   *         not in the hand
   */
  private int twoOfClubsIndex() {
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == Suit.CLUBS
          && getHand().get(i).getRank() == Rank.TWO) {
        return i;
      }

    }

    return -1;
  }

}
