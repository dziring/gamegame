package edu.brown.cs.group.player;

/**
 * A class that represents a human player.
 */
public class HumanPlayer extends Player {

  /**
   * Constructs a human player.
   *
   * @param name
   *          the name of the player.
   */
  public HumanPlayer(String name) {
    super(name);
  }

  public HumanPlayer(String name, int id) {
    super(name, id);
  }
}
