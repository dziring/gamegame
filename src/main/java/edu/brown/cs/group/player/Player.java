package edu.brown.cs.group.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.CardComparator;

/**
 * A class that represents a player who plays cards.
 */
public abstract class Player {

  private String name;
  private int id;
  private List<Card> hand;
  private int score;
  private List<String> info;

  /**
   * Creates a player.
   * 
   * @param name
   *          the name of the player.
   */
  public Player(String name) {
    this.name = name;
    this.hand = new ArrayList<Card>();
    this.score = 0;
    this.info = new ArrayList<String>();
  }

  /**
   * Gets the information of the player.
   * 
   * @return A list of strings that contain information about the player.
   */
  public List<String> getInfo() {
    return ImmutableList.copyOf(info);
  }

  /**
   * Sets the players info.
   * 
   * @param info
   *          a list of strings that contain information about the player.
   */
  public void setInfo(List<String> info) {
    this.info = info;
  }

  public Player(String name, int id) {
    this(name);
    this.id = id;
  }

  /**
   * Constructs a player with all the necessary parts. (Used to make an AI
   * player when a human player drops out of a game).
   *
   * @param name
   *          The name of the player
   * @param id
   *          The id of the player
   * @param hand
   *          The player's hand
   * @param score
   *          The player's current score
   */
  protected Player(String name, int id, List<Card> hand, int score) {
    this.name = name;
    this.id = id;
    this.hand = hand;
    this.score = score;
  }

  /**
   * Returns the id of the player.
   *
   * @return the id of the player, between 0 and 3 (inclusive).
   */
  public int getID() {
    return id;
  }

  /**
   * Sets the id of the player.
   *
   * @param newID
   *          The new ID for the player
   */
  public void setID(int newID) {
    this.id = newID;
  }

  /**
   * Returns the hand of the player.
   *
   * @return a list that contains the hand of the player, the list contains
   *         cards.
   */
  public ImmutableList<Card> getHand() {
    return ImmutableList.copyOf(hand);
  }

  /**
   * Determines if the player has any cards left in the hand.
   *
   * @return true if there are cards left, false otherwise.
   */
  public boolean hasCards() {
    return hand.size() > 0;
  }

  /**
   * Determines the current score of the player in the game.
   *
   * @return the int representing the current score of the game.
   */
  public int getScore() {
    return score;
  }

  /**
   * Adds a delta score to the total score of the player.
   *
   * @param delta
   *          the score to add or subtract from the player's score.F
   * @return the new score of the player after the delta addition.
   */
  public int addToScore(int delta) {
    score += delta;
    return score;
  }

  /**
   * Adds a card to the hand of the player, in no particular order.
   *
   * @param card
   *          the card to be added to the hand of the player.
   */
  public void addCard(Card card) {
    hand.add(card);
    Collections.sort(hand, new CardComparator());
  }

  /**
   * Removes a card from the hand of the player.
   *
   * @param card
   *          the card to be removed from the player.
   */
  public void removeCard(Card card) {
    hand.remove(card);
  }

  /**
   * Return but do not remove the card at the given position.
   *
   * @param position
   *          the position of the card in the hand.
   * @return a card the card that is in the hand of the player at the given
   *         input position.
   */
  public Card getCard(int position) {
    return hand.get(position);
  }

  /**
   * Removes a card from the hand of the player where the card is at a given
   * index in the hand.
   *
   * @param position
   *          The index of the card to be removed.
   * @return The card that was removed from the hand.
   */
  public Card removeCard(int position) {
    return hand.remove(position);
  }

  /**
   * Determines if the player has a card of a particular suit in the hand.
   *
   * @param suit
   *          the suit to be checked.
   * @return true if the player has the card of the input suit, false otherwise.
   */
  public boolean hasSuit(Card.Suit suit) {
    for (Card card : hand) {
      if (card.getSuit().equals(suit)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Determines the number of cards in the players hand with the input suit.
   *
   * @param suit
   *          the suit to be checked.
   * @return the number of cards in the players hand with the inout suit.
   */
  public int numberOfSuit(Card.Suit suit) {
    int counter = 0;
    for (Card card : hand) {
      if (card.getSuit().equals(suit)) {
        counter++;
      }
    }
    return counter;
  }

  /**
   * Determines the number of cards in the player's hand.
   *
   * @return the number of cards in the players hand.
   */
  public int handSize() {
    return hand.size();
  }

  @Override
  public String toString() {
    return name.toString() + "(id:" + id + ")";
  }

  /**
   * Returns the player's name.
   *
   * @return The player's name
   */
  public String getName() {
    return name;
  }

  /**
   * Return whether or not this is an AI player.
   *
   * @return true if this is an AI player, otherwise false.
   */
  public boolean isAI() {
    return false;
  }

  @Override
  public int hashCode() {
    return this.id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (!(o instanceof Player)) {
      return false;
    }

    Player other = (Player) o;

    return (this.id == other.id);
  }
}
