package edu.brown.cs.group.player;

import java.util.ArrayList;
import java.util.List;

import edu.brown.cs.group.cardGame.Game;
import edu.brown.cs.group.cardGame.PlayerCard;
import edu.brown.cs.group.cardGame.Setback;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.CardComparator;

/**
 * A class that represents a setback player.
 *
 */
public class SetbackAIPlayer extends AIPlayer {
  private Suit myTrump;
  private CardComparator compare;

  /**
   * Constructs a setback player with a name.
   * 
   * @param name
   *          the name of the player.m
   */
  public SetbackAIPlayer(String name) {
    super(name);
    compare = new CardComparator();
  }

  public SetbackAIPlayer(String name, int id, List<Card> hand, int score) {
    super(name, id, hand, score);
    compare = new CardComparator();
  }

  @Override
  public Card generateMove(Game currentGame) {
    Setback currentSetbackGame = (Setback) currentGame;

    // If it's the first round of the game and you're the first player, play the
    // highest card of the trump suit
    if (getHand().size() == 6) {
      if (currentSetbackGame.getCurrentOnSuit() == null) {
        return getHand().get(playHighest(myTrump));
      } else {

        // If it's the first round and you're not the first player, play a
        // middle
        // card of the trump suit unless you have one higher than what was
        // played
        Card highestInPot = highestTrumpInPot(currentGame.getPot());
        int myHighest = playHighest(currentSetbackGame.getTrump());
        Card myHighestCard = getHand().get(myHighest);
        int compareVal = compare.compare(myHighestCard, highestInPot);
        if (compareVal == 1) {
          return myHighestCard;
        } else {
          List<Card> valid = validMoves(currentGame.getCurrentOnSuit(),
              currentGame.getTrump());
          return valid
              .get(playAMiddleCard(valid, currentSetbackGame.getTrump()));

        }

      }

    } else {
      // If you're the first player of another round, maybe like play a high
      // card but maybe not of the trump suit
      if (currentSetbackGame.getCurrentOnSuit() == null) {
        return getHand()
            .get(playHighestNotTrump(currentSetbackGame.getTrump()));

      } else {
        if (currentSetbackGame.getCurrentOnSuit() != currentSetbackGame
            .getTrump()) {
          if (getHand().size() <= 3) {
            // Try to play a jack of the trump suit later in the game, and when
            // the
            // onSuit isn't the trump suit
            int indexOfJack = indexOfJack(currentSetbackGame.getTrump());
            if (indexOfJack != -1) {
              return getHand().get(indexOfJack);
            }
          }
        }

        // Otherwise, look at the pot and if there are good things in it (tens)
        // and
        // the onSuit isn't the trump suit, use your trump suit cards to get
        // them
        for (int i = 0; i < currentSetbackGame.getPot().size(); i++) {
          if (currentSetbackGame.getPot().get(i).getCard().getRank() == Rank.TEN) {
            if (playHighestTrump(currentSetbackGame.getTrump()) != -1) {
              return getHand().get(
                  playHighestTrump(currentSetbackGame.getTrump()));
            }

          }
        }

      }

      // if there are good trump suit cards (low card, high card) play a high
      // trump suit card to get them
      if (potContainsLowCards(currentSetbackGame.getPot(),
          currentSetbackGame.getTrump())) {
        if (playHighestTrump(currentSetbackGame.getTrump()) != -1) {
          return getHand().get(playHighestTrump(currentSetbackGame.getTrump()));
        }

      }

      // otherwise play the onsuit if you can.
      if (playOnSuit(currentSetbackGame.getCurrentOnSuit()) != -1) {
        return getHand().get(playOnSuit(currentSetbackGame.getCurrentOnSuit()));
      }

      // otherwise play anything that's not a ten, preferably
      if (justDontPlayATen() != -1) {
        return getHand().get(justDontPlayATen());
      }

    }

    return getHand().get(generateNextInt(getHand().size()));
  }

  private int justDontPlayATen() {
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getRank() != Rank.TEN) {
        return i;
      }
    }

    return -1;
  }

  private int playOnSuit(Suit onSuit) {
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == onSuit) {
        return i;
      }
    }

    return -1;
  }

  private int playHighestTrump(Suit trump) {
    int highestTrump = -1;
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == trump) {
        if (highestTrump == -1) {
          highestTrump = i;
        } else {
          int compareVal = compare.compare(getHand().get(i),
              getHand().get(highestTrump));
          if (compareVal == 1) {
            highestTrump = i;
          }
        }
      }
    }

    return highestTrump;
  }

  private boolean potContainsLowCards(List<PlayerCard> pot, Suit trump) {
    for (PlayerCard p : pot) {
      if (p.getCard().getSuit() == trump) {
        if (p.getCard().getRank() == Rank.TWO
            || p.getCard().getRank() == Rank.THREE
            || p.getCard().getRank() == Rank.FOUR) {
          return true;
        }
      }
    }

    return false;
  }

  private int playAMiddleCard(List<Card> valid, Suit trump) {
    int firstCardNotTrump = 0;
    boolean hasNotTrump = false;

    for (int i = 0; i < valid.size(); i++) {
      if (valid.get(i).getSuit() != trump) {
        firstCardNotTrump = i;
        hasNotTrump = true;
        break;
      }

    }

    int highest = firstCardNotTrump;
    int lowest = firstCardNotTrump;
    int middle = firstCardNotTrump;

    for (int i = firstCardNotTrump; i < valid.size(); i++) {
      if (valid.get(i).getSuit() != trump || !hasNotTrump) {
        int compareHighest = compare.compare(valid.get(i), valid.get(highest));
        if (compareHighest == 1) {
          highest = i;
        } else {
          int compareLowest = compare.compare(valid.get(i), valid.get(lowest));
          if (compareLowest == -1) {
            lowest = i;
          } else {
            middle = i;
          }
        }

      }
    }

    return middle;

  }

  private Card highestTrumpInPot(List<PlayerCard> pot) {
    Card highest = pot.get(0).getCard();
    Suit trump = highest.getSuit();
    for (PlayerCard c : pot) {
      if (c.getCard().getSuit() == trump) {
        int compareVal = compare.compare(c.getCard(), highest);
        if (compareVal == 1) {
          highest = c.getCard();
        }
      }
    }

    return highest;

  }

  private int playHighestNotTrump(Suit trump) {
    Suit toPlay = trump;
    int highest = 0;
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() != trump) {
        toPlay = getHand().get(i).getSuit();
        highest = i;
        break;
      }
    }

    for (int i = highest; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == toPlay) {
        int compareVal = compare.compare(getHand().get(i),
            getHand().get(highest));
        if (compareVal == 1) {
          highest = i;
        }
      }
    }

    return highest;

  }

  private int playHighest(Suit s) {
    int highestOfSuit = 0;
    int j = getHand().size();

    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == s) {
        highestOfSuit = i;
        j = i;
        break;
      }
    }

    for (int i = j; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == s) {
        int compareVal = compare.compare(getHand().get(i),
            getHand().get(highestOfSuit));
        if (compareVal == 1) {
          highestOfSuit = i;
        }

      }

    }

    return highestOfSuit;
  }

  @Override
  public int makeBet(Game currentGame) {
    int highestBet = 0;
    myTrump = Suit.SPADES;

    for (Suit s : Suit.values()) {
      int thisSuitsBet = countSuit(s);
      if (thisSuitsBet > highestBet) {
        highestBet = thisSuitsBet;
        myTrump = s;
      }
    }

    return highestBet;

  }

  private int countSuit(Suit currSuit) {

    // if of any particular suit, you have three of the following:
    // an ace, king, or queen
    // a two or three of that suit
    // a jack
    // at least two other cards of that suit
    // bet three

    boolean foundHighCard = false;
    int highStakesCount = 0;
    int otherCount = 0;

    for (Card c : getHand()) {

      if (c.getSuit() == currSuit) {

        if ((c.getRank() == Rank.QUEEN || c.getRank() == Rank.KING || c
            .getRank() == Rank.ACE)) {
          if (!foundHighCard) {
            highStakesCount++;
          } else {
            otherCount++;
          }
        } else if (c.getRank() == Rank.JACK) {
          highStakesCount++;
        } else if (c.getRank() == Rank.TEN) {
          highStakesCount++;
        } else {
          otherCount++;
        }

      }

    }

    if (otherCount >= 2) {
      highStakesCount++;
    }

    if (highStakesCount > 3) {
      return 3;
    } else if (highStakesCount < 2) {
      return 0;
    } else {
      return highStakesCount;
    }

  }

  private List<Card> validMoves(Suit onSuit, Suit trumpSuit) {
    if (onSuit == null) {
      return getHand();
    }

    List<Card> validCards = new ArrayList<Card>();
    for (Card c : getHand()) {
      if (c.getSuit() == onSuit || c.getSuit() == trumpSuit) {
        validCards.add(c);
      }
    }

    if (validCards.size() == 0) {
      return getHand();
    }

    return validCards;

  }

  private int indexOfJack(Suit trump) {
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == trump
          && getHand().get(i).getRank() == Rank.JACK) {
        return i;
      }
    }

    return -1;

  }

}
