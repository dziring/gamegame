package edu.brown.cs.group.player;

import java.util.List;

import edu.brown.cs.group.cardGame.Game;
import edu.brown.cs.group.cardGame.Spades;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.games.CardComparator;

/**
 * A class that represents a setback player.
 *
 */
public class SpadesAIPlayer extends AIPlayer {
  private CardComparator compare;

  /**
   * Constructs a spades ai player.
   * 
   * @param name
   *          the name of the player.
   */
  public SpadesAIPlayer(String name) {
    super(name);
    this.compare = new CardComparator();
  }

  public SpadesAIPlayer(String name, int id, List<Card> hand, int score) {
    super(name, id, hand, score);
    this.compare = new CardComparator();
  }

  @Override
  public Card generateMove(Game currentGame) {
    Spades currentSpadesGame = (Spades) currentGame;

    Suit onSuit = currentSpadesGame.getCurrentOnSuit();

    if (onSuit == null) {
      // play anything, but if it's spades be careful
      if (indexOfAceOfSpades() != -1) {
        return getHand().get(indexOfAceOfSpades());
      } else {
        return getHand().get(playNotSpadesIfPossible());
      }

    } else {

      // play the on suit, if you have it and it's not spades
      if (onSuit != Suit.SPADES) {
        int cardToPlay = playOnSuit(onSuit);
        if (cardToPlay != -1) {
          return getHand().get(cardToPlay);
        } else {
          // if you don't have it, play anything, being careful about when you
          // play
          // spades
          // TODO actually make it so that you look at the spades in the pot and
          // decide
          return getHand().get(playNotSpadesIfPossible());

        }

      } else {
        // The on suit is spades. Play a spades and be careful about it
        // So

        // If there's a spades bigger than any of yours in the pot, play your
        // smallest spades
        Integer myBiggestSpades = null;
        for (int i = 0; i < getHand().size(); i++) {
          if (getHand().get(i).getSuit() == Suit.SPADES) {
            myBiggestSpades = i;
            break;
          }
        }

        Card potsBiggestSpades = currentSpadesGame.getPot().get(0).getCard();
        for (int i = 0; i < currentSpadesGame.getPot().size(); i++) {
          int compareVal = compare.compare(currentSpadesGame.getPot().get(i)
              .getCard(), potsBiggestSpades);
          if (compareVal == 1) {
            potsBiggestSpades = currentSpadesGame.getPot().get(i).getCard();
          }
        }

        if (!(myBiggestSpades == null)) {
          int compareSpades = compare.compare(potsBiggestSpades,
              getHand().get(myBiggestSpades));
          if (compareSpades == 1) {
            return getHand().get(getSmallestSpades());
          } else {
            // If there are two or three cards in the pot and you have spades
            // bigger
            // than the one in the pot, play it
            if (currentSpadesGame.getPot().size() > 1) {
              return getHand().get(myBiggestSpades);

            } else {
              // If there's one card in the pot, play a small spades because you
              // don't know what's gonna happen
              return getHand().get(getSmallestSpades());

            }

          }

        }

      }
    }
    return getHand().get(0);
  }

  /**
   * Returns the index of the smallest spades card in the Spades AI Player's
   * hand.
   *
   * @return The integer index of the smallest spades card in the spades AI
   *         player's hand or the first card in the hand if there are no spades.
   * 
   */
  public Integer getSmallestSpades() {
    Integer smallestSpades = null;
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == Suit.SPADES) {
        if (smallestSpades == null) {
          smallestSpades = i;
        } else {
          int compareVal = compare.compare(getHand().get(smallestSpades),
              getHand().get(i));
          if (compareVal == 1) {
            smallestSpades = i;
          }
        }
      }

    }

    if (smallestSpades == null) {
      return 0;
    }

    return smallestSpades;

  }

  /**
   * Finds the ace of spades in a the Spades AI Player's hand if it exists.
   *
   * @return the integer index of the ace of spades in the player's hand or -1
   *         if the player doesn't have the ace of spades.
   *
   */
  public int indexOfAceOfSpades() {
    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == Suit.SPADES
          && getHand().get(i).getRank() == Rank.ACE) {
        return i;
      }
    }

    return -1;
  }

  /**
   * When a player, doesn't have a card of the on suit, they can play any card.
   * This method seeks to find a card not of the on suit that is not a spades
   * card, but if the player only has spades it returns the lowest spades card
   * in their hand.
   * 
   * @return The index of the card specified above in the SpadesAIPlayer's hand
   */
  public int playNotSpadesIfPossible() {
    // TODO make it look at the pot and your lowest spades should be bigger than
    // any in the pot if you're the 3rd or fourth player
    int indexOfLowestSpades = 0;

    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() != Suit.SPADES) {
        return i;
      } else {
        int compareVal = compare.compare(getHand().get(i),
            getHand().get(indexOfLowestSpades));
        if (compareVal == -1) {
          indexOfLowestSpades = i;
        }

      }

    }

    return indexOfLowestSpades;

  }

  /**
   * Returns the index in the SpadesAIPlayer's hand of a card in the on suit, or
   * -1 if the AI player doesn't have a card of the on suit in their hand.
   * 
   * @param onSuit
   *          The on suit of this round
   * @return The index of a card in the on suit to play, or -1 if the player
   *         doesn't have a card of the on suit in their hand
   */
  public int playOnSuit(Suit onSuit) {

    for (int i = 0; i < getHand().size(); i++) {
      if (getHand().get(i).getSuit() == onSuit) {
        return i;
      }
    }

    return -1;
  }

  @Override
  public int makeBet(Game currentGame) {
    int bet = 0;
    for (Card c : getHand()) {
      if (c.getSuit() == Suit.SPADES) {
        switch (c.getRank()) {
        case ACE:
          bet += 13;
          break;
        case TWO:
          bet += 1;
          break;
        case THREE:
          bet += 2;
          break;
        case FOUR:
          bet += 3;
          break;
        case FIVE:
          bet += 4;
          break;
        case SIX:
          bet += 5;
          break;
        case SEVEN:
          bet += 6;
          break;
        case EIGHT:
          bet += 7;
          break;
        case NINE:
          bet += 8;
          break;
        case TEN:
          bet += 9;
          break;
        case JACK:
          bet += 10;
          break;
        case QUEEN:
          bet += 11;
          break;
        case KING:
          bet += 12;
          break;
        default:
          bet += 0;

        }

      }

    }

    return bet / 7;

  }

}
