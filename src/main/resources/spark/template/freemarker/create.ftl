<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<meta name="game_id" content="1">
	<title>Game View</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- External JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/lib/css/bootstrap.min.css"/>
	<script type="text/javascript" src="/js/jquery-2.1.1.js"></script>
	<script type="text/javascript" src="/lib/js/bootstrap.min.js"></script>

	<!-- Our JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<style type="text/css">
	main {
		margin-top: 60px;
		margin-left: 10px;
	}

	form {
		padding: 20px;
	}

	#ownbutton {
		margin-top:50px;
	}

	</style>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
	  <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	      </button>
	      <a class="navbar-brand" href="/home">GameGame</a>
      </div>
      <div class="collapse navbar-collapse">
      	<ul class="nav navbar-nav">
      		<li><a href="/create">Make a New Game</a></li>
      	</ul>
      </div>

	</div>
</nav>

<header></header>

<main class="container">
<div class="row">
	<form method="POST" class="form form-horizontal col-sm-6" action="/create">
		<h2 class="text-center">Select which type of game you would like:</h2>
		<div class="form-group">
			<label class="control-label" ><input type="radio" name="type" value="hearts">
			Hearts</label>
		</div>
		
		<div class="form-group">
			<label class="control-label" ><input type="radio" name="type" value="spades">
			Spades</label>
		</div>
		
		<div class="form-group">
			<label class="control-label"><input type="radio" name="type" value="setback">
			Setback</label>
		</div>
		
		<div class="form-group">
			<label class="control-label" for="name">Enter your game's name here</label>
			<input class="form-control" type="text" name="name" placeholder="Super Great Game"><br>
		</div>
		<div class="form-group">
			<input class="btn btn-success btn-lg btn-block" type="submit">
		</div>
	</form>
	<h3 class="col-sm-2" style="padding-left:3em"><br>OR</h3>
	<div class="col-sm-4" id="ownbutton">
		<a href="/createown" class="btn btn-lg btn-primary" style="padding-top:4em;text-align:center">Create your own!<br><br><br><br></a>
	</div>
</div>
</main>


<footer></footer>


</body>
</html>