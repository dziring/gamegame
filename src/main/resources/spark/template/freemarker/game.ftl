<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<meta name="game_id" content="${id}">
	<title>Game View</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- External JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/lib/css/bootstrap.min.css"/>
	<script type="text/javascript" src="/js/jquery-2.1.1.js"></script>
	<script type="text/javascript" src="/lib/js/bootstrap.min.js"></script>

	<!-- Our JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<script type="text/javascript" src="/js/game.js"></script>

	<style type="text/css">
	* {
		/*border: 1px dashed red;*/
	}

	.player {
		border-radius: 5px;
		height: 2in;
	}

  .player_0_text {
    color: #3c763d;
  }

  .player_1_text {
    color: #8a6d3b;
  }

  .player_2_text {
    color: #a94442;
  }

  .player_3_text {
    color: #337ab7;
  }

  .player_0_bg {
    background-color: #dff0d8;
  }

  .player_1_bg {
    background-color: #fcf8e3;
  }

  .player_2_bg {
    background-color: #f2dede;
  }

  .player_3_bg {
    background-color: #d9edf7;
  }

  li {
    list-style-type: square;
  }

  #hand {
    margin-top: 10px;
  }

	#pot {
    border-bottom : 0px solid #3c763d;
    border-left: 0px solid #8a6d3b;
    border-top: 0px solid #a94442;
    border-right: 0px solid #337ab7;
    padding-bottom: 8px;
    padding-left: 8px;
    padding-top: 8px;
    padding-right: 8px;
		background-color: #f7f7f9;
		border-radius: 5px;
		height: 2in;
	}

  #pot h3 {
    margin-top: 10px;
    margin-left: 5px;
  }

	body {
		padding: 40px;
		padding-top: 70px;
	}

  .message {
    list-style-type: none;
  }

  #messages_list {
    height: 4in;
    overflow: scroll;
    width: 50%;
    min-width: 2in;
  }

  #message_box {
    width: 80%;
  }

  #controls {
    margin-top: 0.15in;
  }

  input[name='tricks'] {
    width: 2cm;
  }

  .unread {
    font-weight: bold;
  }

  .card {
    padding: 5px;
  }

  .cards span {
    display: inline-block;
    width: 84px;
    margin: 2px;
    float: left;
    border-radius: 3px;
  }

  #bad_card_alert {
    height: 1.1cm;
  }

	</style>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
	  <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"></button>
	      <a class="navbar-brand" href="/home">GameGame</a>
      </div>
      <div class="collapse navbar-collapse">
      	<ul class="nav navbar-nav">
      		<li><a href="/create">Make a New Game</a></li>
      	</ul>
      </div>

	</div>
</nav>

<main>
<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#Game" aria-controls="Game" role="tab" data-toggle="tab">Game</a></li>
    <li role="presentation"><a id="messages_tab_link" href="#Messages" aria-controls="Messages" role="tab" data-toggle="tab">Messages  <span id="num_unread" class="badge"></span></a></li>
    <li role="presentation"><a href="#Rules" aria-controls="Rules" role="tab" data-toggle="tab">Rules</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="Game">
    	<div class="panel container" id="game_area">
    		<div class="row">
    			<h2><span class="" id="game_info"></span></h2>
    		</div>

    		<div class="row">
          <div class="player col-sm-4" id="prev_pot">
            <h3 class="name">Previous</h3>
            <div class="cards"></div>
          </div>
    			<div class="player col-sm-4 bg-danger" id="player_2">
    				<h3 class="name player_2_text">....</h3>
    				<h4 class="hand">Hand: ...</h4><br/>
    				<h4 class="other_info"></h4>
    			</div>
    		</div>

    		<div class="row">
    			
	    		<div class="player col-sm-4 bg-warning" id="player_1">
	    			<h3 class="name player_1_text">....</h3>
    				<h4 class="hand">Hand: ...</h4><br/>
    				<h4 class="other_info"></h4>
	    		</div>

	    		<div id="pot" class="col-sm-4">
	    			<h3>POT</h3>
	    			<div class="cards"></div>
	    		</div>

	    		<div class="player col-sm-4 bg-info" id="player_3">
	    			<h3 class="name player_3_text">....</h3>
    				<h4 class="hand">Hand: ...</h4><br/>
    				<h4 class="other_info"></h4>
	    		</div>

    		</div>
    		<div class="row">
	    		<div class="player col-sm-4 col-sm-offset-4 bg-success" id="player_0">
	    			<h3 class="name player_0_text">....</h3>
    				<h4 class="hand">Hand: ...</h4><br/>
            <h4 class="other_info"></h4>
	    		</div>
    		</div>

    		<div class="row" id="controls">
    			<h2 class="col-sm-4">It is <span class="text-info" id="turn_indicator">...'s</span> turn.</h2>
          <div class="col-sm-4 alert alert-danger text-center" id="bad_card_alert" style="display:none;">
            <!-- <button type="button" class="close"><span>&times;</span></button> -->
            <span class="message_content"></span>
          </div>
          <div class="col-sm-3 form-inline">
          <form id="trick_caller">
            <label for="tricks">Make your wager: </label>
            <input type="number" style="width:2cm;" class="form-control" name="tricks"/>
            &nbsp;<input type="submit" class="btn btn-success" value="Call"/>
          </form>
          </div>
        </div>
  			<div class="row well" id="hand">
  			</div>
        <div class="row">
          <a href="/leave" id="leave_button" class="btn btn-danger">Leave Game</a>
        </div>
    	</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="Messages">
      <div class="panel container">
        <ul id="messages_list"></ul>
        <form id="message_form" class="form-inline" onsubmit="event.preventDefault(); send_message();">
        <fieldset>
          <input class="form-control" type="text" name="message" placeholder="Message..." id="message_box"></input>
          <button class="btn btn-success" onclick="send_message()">Send!</button>
        </fieldset>
        </form>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="Rules">
        <!-- Small modal -->
		<div id="settings_loading">
        <p>${rules}</p>
    	</div>
    </div>
  </div>

</div>
	
</main>

<footer>
        <div id="namechooser" class="modal fade in" tabindex="-1" role="dialog" data-show="true" data-backdrop="static">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="modal-title">Please choose a name!</h2>
              </div>
              <div class="modal-body form-inline">
                <input id="namebox" class="form-control" type="text" placeholder="Name..." maxlength="10"/>
                <button class="btn btn-primary" id="name_button" onclick="name_submit($('#namebox').val())">Join</button>
                <div class="player-info">
                  <h4>Players:</h4>
                  <ul class="player-list">
                    <li class="player_0_text"></li>
                    <li class="player_1_text"></li>
                    <li class="player_2_text"></li>
                    <li class="player_3_text"></li>                  
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="playerwaiter" class="modal fade in" tabindex="-1" role="dialog" data-backdrop="static">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="modal-title">Waiting for more players...</h2>
              </div>
              <div class="modal-body">
                <button class="btn btn-danger" onclick="addAI(true)">Add Hard AI Player</button>
                <button class="btn btn-warning" style="margin-top:0.3cm;" onclick="addAI(false)">Add Easy AI Player</button>
                <br/>
                <span id="ai_info" class="text-success"></span>
                <div class="player-info">
                  <h4>Players:</h4>
                  <ul class="player-list">
                    <li class="player_0_text"></li>
                    <li class="player_1_text"></li>
                    <li class="player_2_text"></li>
                    <li class="player_3_text"></li>   
                  </ul>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="gameover" class="modal fade in" tabindex="-1" role="dialog" data-backdrop="static">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="modal-title">Game Over! <span id="winner"></span> wins.</h2>
              </div>
              <div class="modal-body">
                <a href="/home" class="btn btn-success btn-lg">Back to Home</a>
              </div>
            </div>
          </div>
        </div>
</footer>

</body>
</html>