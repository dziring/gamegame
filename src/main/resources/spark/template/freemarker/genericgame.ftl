<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<meta name="game_id" content="1">
	<title>Game View</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- External JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/lib/css/bootstrap.min.css"/>
	<script type="text/javascript" src="/js/jquery-2.1.1.js"></script>
	<script type="text/javascript" src="/lib/js/bootstrap.min.js"></script>

	<!-- Our JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="/css/genericgame.css"/>
	<script type="text/javascript" src="/js/genericgame.js"></script>
	<style type="text/css">
	main {
		margin-top: 60px;
	}

	#genericGame {
		margin: 40px;
	}

	#scoringmethod input {
		width: 2cm;
	}
	li {
		list-style-type: none;
	}
	</style>
</head>

<body style="background-color:#FFFFFF">
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid" style="background-color:#E6E6E6">
	  <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Toggle navigation</span>
	      </button>
	      <a class="navbar-brand" href="/home">GameGame</a>
      </div>
	</div>
</nav>

<header></header>

<main>

	<form method="POST" id="genericGame" action="/createown" class="form-horizontal">
	<h2>Specify your game</h2>
	
	<div class="form-group">
		<label class="control-label col-sm-2" for="gamename">Name </label>
		<div class="col-sm-6">
			<input class='form-control' type="text" id="gamename" name="gamename" placeholder="My Very First Game">
		</div>
	</div>

	<div class="form-group">	
		<label class='control-label col-sm-2' for="maxpoints">Who wins?</label>
		<div class="col-sm-3">
			<select class="form-control" name="maxpoints" id="maxpoints" form="genericGame">
				<option value="true">Highest Score</option>
				<option value="false">Lowest Score</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<br>
		<label class='control-label col-sm-2' for="endsbyround">Ends After </label>
		<label class="form-inline col-sm-4" for="endgamepoint">
			<input class='form-control' style="width:2cm;" type="number" id="endgamepoint" name="endgamepoint" size="3" value="5" required>&nbsp;
			<label><input type="radio" name="endsbyround" value="true" checked> Rounds</label>
			<label><input type="radio" name="endsbyround" value="false"> Points</label>
		</label>
	</div>
	
	<div class="form-group">
		<label class='control-label col-sm-2' for="trumpsuit">Trump Suit </label>
		<div class="col-sm-2">
			<select class="form-control" id="trumpsuit" name="trumpsuit" form="genericGame" onchange="checkIfTrump(this)">
				<option value="none">None</option>
				<option value="diamonds">♦</option>
				<option value="clubs">♣</option>
				<option value="hearts">♥</option>
				<option value="spades">♠</option>
				
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class='control-label col-sm-2 broken' style="display:none;" for="istrumpbroken">Does Trump Need to be Broken?</label>
		<div class="col-sm-2">
			<select class="form-control broken" style="display:none;" name="istrumpbroken" form="genericGame">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class='control-label col-sm-2' for="isacehigh">Is Ace High?</label>
		<div class="col-sm-2">
			<select class="form-control" name="isacehigh" id="isacehigh" form="genericGame">
				<option value="true">Yes</option>
				<option value="false">No</option>
			</select>
		</div>
	</div>
	
	<div class="form-group">
		<label class='control-label col-sm-2' for="cardsinhand">Number of Cards in a Hand</label>
		<div class="col-sm-1">
			<select class="form-control" name="cardsinhand" id="cardsinhand" form="genericGame"></select>
		</div>
	</div>
	<div class="form-group">
		<label class='control-label col-sm-2' for="istrickcalling">Do you call tricks?</label>
		<div class="col-sm-3">
			<select class="form-control" name="istrickcalling" id="istrickcalling" form="genericGame" onchange="trickCalling(this)">
				<option value="false">No (e.g. Hearts) </option>
				<option value="true">Yes (e.g. Spades, Setback)</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<br>
		<label class="form-inline col-sm-4 tricks" style="display:none;" for="tricksmissed"> 
			<input class='form-control tricks' style="width:2cm;display:none" type="number" name="tricksmissed" size="3" value="-10" required>&nbsp;
		Points per trick missed </label>
		<label class="form-inline col-sm-4 tricks" style="display:none;" for="trickscorrect"> 
			<input class='form-control tricks' style="width:2cm;display:none;" type="number" name="trickscorrect" size="3" value="30" required>&nbsp;
		Points per trick correct </label>
		<label class="form-inline col-sm-4 tricks" style="display:none;" for="tricksover"> 
			<input class='form-control tricks' style="width:2cm;display:none;" type="number" name="tricksover" size="3" value="10" required>&nbsp;
		Point per trick over </label>
	</div>
	<script>
		var tempList = $("#cardsinhand")[0]; 
		htmlCode = "";
		for (i = 1; i < 14; i++) { 
    		htmlCode += "<option value=\"" + i + "\">" + i + "</option>";
		}
		tempList.innerHTML = htmlCode;
	</script>
	<br>
	<h3>Cards </h3>
	<p id="pointstext"> Set the points that each card is worth (if taken by a player) by changing its number. </p>
	<p> Unclick checkboxes to exclude cards from the game. </p>
	<ul id="scoringmethod"></ul>
	<script>
		var tempList = $("#scoringmethod")[0]; 
		htmlCode = "";
		suits = ["♦", "♣", "♥" , "♠"]
		for (i = 0; i < 13; i++) { 
			card = ""
			switch(i) {
			    case 9:
			        card = "J"
			        break;
			    case 10:
			        card = "Q"
			        break;
			    case 11:
			        card = "K"
			        break;
			    case 12:
			        card = "A"
			        break;
			    default:
			        card = String(i + 2)
			}

    		htmlCode += "<li class='form-group' row='"+i+"'>"
    		+ "<div class='form-inline'>"

    		for (j = 0; j < 4; j++) { 
	    		htmlCode += "<label class='control-label cardlabel' for=\"scoringmethod" + (i + 13*j)
	    		+ "\" style=\"color:" + (!(j % 2) ? "#C80000" : "black") + ";width:155px;text-align:left\">"
	    		+ "<font size=\"5\">" + card + " " + suits[j] + "&nbsp;</font>"
	    		+ "<input class='form-control' type=\"number\" name=\"scoringmethod" + (i + 13*j) + "\" id=\"scoringmethod"
	    		+ (i + 13*j) + "\" size=\"3\" value=\"0\" required>&nbsp;&nbsp;&nbsp;&nbsp;"
	    		+ "<input class=\"include tcheckbox\" type=\"checkbox\" name=\"include" + (i + 13*j) + "\" value=\"" + (i + 13*j)
	    		+ "\" id=\"include" + (i + 13*j) + "\" onchange=\"showPointBox(this, 'true')\" checked></label>"
    		}
    		htmlCode += "&nbsp;&nbsp;&nbsp;&nbsp;"
    		htmlCode += "<label class='control-label' for=\"exclude" + i + ">"
	    		+ "<font size=\"3\"> Apply to all: " + card + "&nbsp;</font>"
	    		+ "<input class=\"include\" type=\"checkbox\" name=\"exclude" + i + "\" value=\"" + i
	    		+ "\" id=\"exclude" + i + "\" onchange=\"applyAll(this)\" checked></label>"
    		htmlCode += "</div></li>"
		}
		tempList.innerHTML = htmlCode;
	</script>
	<div class="form-group">
		<div class="col-sm-8 col-sm-offset-1">
			<input class="btn btn-lg btn-success btn-block col-sm-5 col-sm-offset-1" type="submit">
		</div>
	</div>
	</form>


</main>



<footer></footer>


</body>
</html>