<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'/>
	<title>${title}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- External JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/lib/css/bootstrap.min.css"/>
	<script type="text/javascript" src="/js/jquery-2.1.1.js"></script>
	<script type="text/javascript" src="/lib/js/bootstrap.min.js"></script>

	<!-- Our JS/CSS -->
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<script type="text/javascript" src="/js/main.js"></script>
	<script type="text/javascript">
	function show_games() {
		$("#cardsimage").fadeOut();
		$("#existing_button").fadeOut();
		$("#current_games").fadeIn();
	}

	function update_games() {
		$.get("/games", null, function(data, status) {
			console.log("Games update response: " + status);
			if(status == "success") {
				$("#games_list").html(data);
			}
		});
	}

	$(document).ready(function() {
		$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'top'});
		update_games();

		window.setInterval(update_games, 10000);
	});
	</script>
	<style type="text/css">
	.game {
		text-align: left;
	}

	</style>
</head>
<body>
<nav></nav>
<header class="container"></header>
<div class="jumbotron text-center">
	<h1>Welcome to GameGame!</h1>
	<p>To get started, please choose a name and start browsing the games available, or make your own!</p>
</div>
<section class="container" align="center">
<a href="/create" class="btn btn-success btn-lg">Make a new game! <span class="glyphicon glyphicon-play-circle"></span></a><span id="existing_button"> or 
<button onclick="show_games()" class="btn btn-primary btn-lg">Join an existing game! <span class="glyphicon glyphicon-play-circle"></span></button></span>
 

<div id="current_games" style="display:none;">
<hr/>

<em class="text-muted">Updates every ten seconds...
<br/>
</em>

	<table class="table table-striped table-hover">
	<thead><tr><th>ID</th><th>Name</th><th>Kind</th><th>Players <a id="refresh_button" class="btn btn-default" onclick="update_games()" alt="Refresh"><span class="glyphicon glyphicon-refresh"></span></a></th></tr></thead>
	<tbody id="games_list">
	<tr class="game"><td class="idstring">1234</td>   <td class="name"><a href="/game/1">Example</a></td>   <td class="kind">Setback</td>   <td class="players">0</td>   </tr>

	<tr class="game"><td class="idstring">3456</td>   <td class="name">Example</td>   <td class="kind">Hearts</td>   <td class="players">0</td>   </tr>

	<tr class="game"><td class="idstring">4354</td>   <td class="name">Example</td>   <td class="kind">Spades</td>   <td class="players">0</td>   </tr>

	<tr class="game"><td class="idstring">2354</td>   <td class="name">Example</td>   <td class="kind"><a href="#" data-toggle="popover" title="Custom Game" data-content="Describe rules in brief here">Custom</a></td>   <td class="players">0</td>   </tr>

	<tr class="game">   <td class="idstring">3452</td>   <td class="name">Example</td>   <td class="kind">Setback</td>   <td class="players">0</td>   </tr>

	<tr class="game">   <td class="idstring">7654</td>   <td class="name">Example</td>   <td class="kind">Setback</td>   <td class="players">0</td>   </tr>

	</tbody>
		
	</table>

</div>
</section>
<br>
<div align="center">
	<img id="cardsimage" src="/images/CS32Cards.png" alt="CS32 Cards" style="width:344px;height:325px">
</div>

<footer></footer>
</body>
</html>