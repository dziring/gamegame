
var TURN_URL;
var JOIN_URL;
var JOIN_AI_URL;
var MESSAGE_URL;
var CALL_TRICKS_URL;

var game_id = $("meta[name='game_id']").attr('content');
var players = {};

var thisPlayer = -1;

var started = false;

var spectating = false;
var spectatorName;

var lastUpdateMsg = 0;
var lastUpdateGame = 0;

var unreadMessages = 0;
var messageWindowShown = false;

var prev_num_bets = 4;

var pot_css = {
	"border-bottom-width"  : "0px",
	"border-bottom-color"  : "#3c763d",
	"border-bottom-style"  : "solid",
	// "border-bottom"  : "0px solid #3c763d",

    "border-left-width" : "0px",
    "border-left-color" : "#8a6d3b",
    "border-left-style" : "solid",
    // "border-left" : "0px solid #8a6d3b",

    "border-top-width" : "0px",
    "border-top-color" : "#a94442",
    "border-top-style" : "solid",
    // "border-top" : "0px solid #a94442",

    "border-right-width" : "0px",
    "border-right-color" : "#337ab7",
    "border-right-style" : "solid",
    // "border-right" : "0px solid #337ab7",

    "padding-bottom" : "8px",
    "padding-left" : "8px",
    "padding-top" : "8px",
    "padding-right" : "8px",
}

var upd8;

$(document).ready(function() {

	$("#namechooser").modal("show");


	TURN_URL = "/game/" + game_id + "/turn";
	JOIN_URL = "/game/" + game_id + "/join";
	JOIN_AI_URL = "/game/" + game_id + "/aijoin";
	MESSAGE_URL = "/game/" + game_id + "/messages";
	CALL_TRICKS_URL = "/game/" + game_id + "/calltricks";

	update_game();

	upd8 = setInterval(update_game, 500);

	$("#trick_caller").submit(call_tricks);
	$("#trick_caller input[name='tricks']").change(function() {
		$(this).removeClass("has-error");
	});

	var unreadInterval = null;

	$('#messages_tab_link')
	.on('shown.bs.tab', function (e) {
  		$("#num_unread").text("");
  		unreadMessages = 0;
  		$("#messages_list li:last-child")[0].scrollIntoView();
  		messageWindowShown = true;

  		unreadInterval = setInterval(function() {
  			$("#messages_list li.unread").removeClass("unread");
  		}, 1000);
	})
	.on('hidden.bs.tab', function(e) {
		messageWindowShown = false;
		clearInterval(unreadInterval);
	});
});

var player_colors = {
	0 :"#3c763d",
	1 :"#8a6d3b",
	2 :"#a94442",
	3 :"#337ab7",
}
  
var player_dirs = {
	0 :"bottom",
	1 :"left",
	2 :"top",
	3 :"right",
}

function send_message() {
	var messageText = $("#message_box").val();
	$("#message_box").val("");

	if(messageText == "") {
		return;
	}

	$.post(MESSAGE_URL, {"playerID": thisPlayer, "message":messageText});
}

function update_game() {
	$.get("/game/"+game_id+"/update", {"t": lastUpdateGame}, function(data, status) {

		if(status != "success") {
			return;
		}

		lastUpdateGame = data.game.lastChanged;

		players = data.game.players;
		if(players[0] && players[1] && players[2] && players[3]) {
			if(!started)  {
				$("#playerwaiter").modal("hide");
				started = true;
			}
			$("#name_button").removeClass("btn-primary").addClass("btn-warning").text("Spectate");
		}

		for(var p in players) {
			var str = players[p].name;
			if(players[p].generator) {
				str += " (AI)";
			}
			$(".player-info .player-list .player_" + p + "_text").text(str)
		}

		if(started) {
			if(data.isOver) {
				clearInterval(upd8);
				$("#gameover").modal("show");
				$("#winner").text(data.winner);
			}
			for(var pid in players) {
				if (data.game.hasTeams) {
					if (pid % 2 == 0) {
						$("#player_" + pid + " .name").text(players[pid].name + " (" + data.game.team1score + ")");
					} else {
						$("#player_" + pid + " .name").text(players[pid].name + " (" + data.game.team2score + ")");
					}
					
				} else {
					$("#player_" + pid + " .name").text(players[pid].name + " (" + players[pid].score + ")");
				}
				
				$("#player_" + pid + " .hand").text("Hand: " + players[pid].hand.length + " cards");
			}

			if(!spectating && data.game.whoseTurn == thisPlayer) {
				$("#turn_indicator").text("YOUR");
			} else {
				$("#turn_indicator").text(players[data.game.whoseTurn].name + "'s");
			}

			$("#turn_indicator").removeClass();
			$("#turn_indicator").addClass("player_" + data.game.whoseTurn + "_text")

			$("#pot .cards").html("");
			resetPotBorders();
			for (var i = 0; i < data.game.pot.length; i++) {
				var playercard = data.game.pot[i];
				var c = new Card(playercard.card.suit, playercard.card.rank);
				$("#pot .cards").append("<span class='player_"+playercard.player.id+"_text player_"+playercard.player.id+"_bg'>" + playercard.player.name+": " + c.asHtml() +"</span>");
				addPotBorder(playercard.player.id);
			};

			$("#pot").animate(pot_css, 200, "linear");

			if(data.game.previousPot) {
				$("#prev_pot .cards").html("");
				for (var i = 0; i < data.game.previousPot.length; i++) {
					var playercard = data.game.previousPot[i];
					var c = new Card(playercard.card.suit, playercard.card.rank);
					$("#prev_pot .cards").append("<span class='player_"+playercard.player.id+"_text player_"+playercard.player.id+"_bg'>" + playercard.player.name+": " + c.asHtml() +"</span>");
				};
				if(data.game.previousWinner != -1) {
					$("#prev_pot .name").html("Previous (went to <span class='player_" + data.game.previousWinner
						+ "_text'>" + players[data.game.previousWinner].name + "</span>)");
				}
			}
			show_hand();

			if(!spectating && data.game.callsTricks) {
				if(data.game.tricksCalled[thisPlayer] == undefined) {
					$("#hand button").attr("disabled", "disabled");
					$("#trick_caller").fadeIn();
				} else {
					$("#hand button").removeAttr("disabled");
				}
				var l = {0: false, 1: false, 2:false, 3:false};
				for(var p in data.game.tricksCalled) {
					$("#player_" + p + " .other_info").text("Wager: " + data.game.tricksCalled[p]);
					l[p] = true;
				}
				for(var w in l) {
					if(!l[w]) $("#player_"+w+" .other_info").text("");
				}
			} else {
				$("#trick_caller").fadeOut();
			}
			$("#game_info").text(data.game.name + "! " + data.game.info);

		}

	});

	$.get(MESSAGE_URL, {"t":lastUpdateMsg}, function(data, status) {
		if(status == "success") {
			$.each(data, function(index, element) {
				var name = element.senderID == -1 ? "System" : players[element.senderID].name;
				var time = new Date(element.timeStamp);

				if(lastUpdateMsg < element.timeStamp) lastUpdateMsg = element.timeStamp;

				$("#messages_list").append("<li class='message unread'><strong>" + name + ": </strong>" + element.message + "<span class='pull-right'>" + time.toLocaleString() +"</span></li>");
				$("#messages_list li:last-child")[0].scrollIntoView();
				if(!messageWindowShown && element.senderID != -1) unreadMessages++;

				$("#num_unread").text(unreadMessages == 0 ? "" : unreadMessages);
			});
		}
	});
}

function call_tricks(event) {
	event.preventDefault();

	var wager = $(event.target).find("input[name='tricks']").val();
	$.post(CALL_TRICKS_URL, {"playerID": thisPlayer, "tricks": wager}, function(data, status) {
		if(status == "success") {
			$("#trick_caller").fadeOut();
		} else {
			$("#trick_caller").addClass("has-error");
		}
	});
}

function show_hand () {
	if(thisPlayer == -1) return;
	var handButtons = $("#hand");
	handButtons.html("");
	var cards = players[thisPlayer].hand;
	for (var i = 0; i < cards.length; i++) {
		var c = new Card(cards[i].suit, cards[i].rank);
		c.n = i;
		handButtons.append(c.asHtml("button"));
	};

	$("#hand button").tooltip({
		"title" : "Play this card",
		"delay" : {
		 	"show" : 600,
			"hide" : 0
		},
	});

	$("#hand button").addClass("btn btn-default");
	$("#hand button").click(function() {
		play_card(this);
	});
}

function play_card (btn) {
	$(btn).attr("disabled", "disabled");
	var suit = $(btn).attr('suit');
	var rank = $(btn).attr('rank');

	var c = $(btn).attr('n');

	$.post(TURN_URL, {"playerID":thisPlayer, "card":c}, function(data, status) {
		if(status != "success") {
			$("#bad_card_alert .message_content").text("Can't play that card!")
			$("#bad_card_alert").fadeIn(600, function() {
				$("#bad_card_alert").delay(500).fadeOut(600);
			});

		} else {
			update_game();
		}
	});
}


function name_submit (name) {
	if(name) {
		$.post(JOIN_URL, {"name": name}, function(data, status) {
			var id = data.playerID;
			thisPlayer = id;
			if(id >= 0) {
				var p = new Player(id, data.name);
				players[id] = p;
				$("#leave_button").attr("href", "/game/" + game_id + "/leave?playerID=" + thisPlayer);
			} else {
				spectate(name);
			}
			$("#namechooser").modal("hide");
		    if(!started) $("#playerwaiter").modal("show");
			update_game();
		});
	}
}

function spectate (name) {
	$("#trick_caller,#hand,#leave_button").fadeOut();
	$("#bad_card_alert .message_content").text("You are spectating on this game.");
	$("#bad_card_alert").fadeIn();

	$("#message_form fieldset").attr("disabled", "true");
	spectating = true;
	spectatorName = name;
}

function resetPotBorders () {
	pot_css["border-bottom-width"] = "0px";
    pot_css["border-left-width"] = "0px";
    pot_css["border-top-width"] = "0px";
    pot_css["border-right-width"] = "0px";
    pot_css["padding-bottom"] = "8px";
    pot_css["padding-left"] = "8px";
    pot_css["padding-top"] = "8px";
    pot_css["padding-right"] = "8px";
}

function addPotBorder (p) {
	pot_css["border-"+player_dirs[p]+"-width"] = "8px";
	pot_css["padding-"+player_dirs[p]] = "0px";
}

function Player(id, name) {
	this.id = id;
	this.name = name;
	this.hand = [];
	this.lastPlayed = null;
	this.info = "";
}

var Rank = {
	TWO : 2,
	THREE : 3,
	FOUR : 4,
	FIVE : 5,
	SIX : 6,
	SEVEN : 7,
	EIGHT : 8,
	NINE : 9,
	TEN : 10,
	JACK : 11,
	QUEEN : 12,
	KING : 13,
	ACE : 14
}
function Card(suit, rank) {
	this.suit = suit;
	this.rank = rank;
	this.rankInt = Rank[this.rank];
}

Card.prototype.getScoringIndex = function() {
	var index = this.rankInt - 2;

    switch(this.suit) {
    	case "DIAMONDS":
    		index += 0;
    		break;
    	case "CLUBS":
    		index += 13;
    		break;
    	case "HEARTS":
    		index += 26;
    		break;
    	case "SPADES":
    		index += 39;
    		break;
    	default:
    		throw "Bad suit";
    }

    return index;
};

Card.prototype.getImageURI = function() {
	var index = this.getScoringIndex();
	return "/images/card_pics/card" + index + ".png";
};

Card.prototype.asHtml = function(element) {
	element = element ? element : "div";
	var nstring = this.n != undefined ? "n='"+this.n+"'" : "";
	var html = "<"+element+" class='card' rank='"+this.rank+"' suit='"+this.suit+"'"+ nstring +">"
	html += "<img src='" + this.getImageURI() + "' /></"+element+">"
	return html;
};


Card.prototype.isRed = function() {
	return this.suit == "HEARTS" || this.suit == "DIAMONDS";
};

Card.prototype.isBlack = function() {
	return this.suit == "CLUBS" || this.suit == "SPADES";
};

Card.prototype.isFaceCard = function() {
	return this.rankInt > 10;
};

function addAI (isHard) {
	$.post(JOIN_AI_URL, {"isHard": isHard}, function(data, status) {
		$("#ai_info").text("Added AI player ID#" + data.playerID + ".");
	});
}

function join_team () {
	$.post(JOIN_URL, {"name": "Louisa"}, log_data);
	$.post(JOIN_URL, {"name": "Dan"}, log_data);
	$.post(JOIN_URL, {"name": "Samier"}, log_data);
}

function log_data (data, status) {
	console.log(data);
}



