$(document).ready(function() {

	$("#endgamepoint").keydown(function (event) {
	    if (event.shiftKey) {
	        event.preventDefault();
	    }
	    if ((event.keyCode < 48 || event.keyCode > 57) && !(event.keyCode == 8)) {
	    	event.preventDefault();
	    }
	});

	for (i = 0; i < 52; i++) { 
		$("#scoringmethod" + i).keydown(function (event) {
		    if (event.shiftKey) {
		        event.preventDefault();
		    }
		    if ((event.keyCode < 48 || event.keyCode > 57)
		    	&& !(event.keyCode == 8) && !(event.keyCode == 9)
		    	&& !(event.keyCode == 189 && event.target.value.length == 0)) {
		    	event.preventDefault();
		    }
		});
	}
});

function checkIfTrump(element) {
	if (element.value == "none") {
    	$(".broken").fadeOut();
    }
    else {
    	$(".broken").fadeIn();
    }
}

function trickCalling(element) {
	if (element.value == "true") {
		for (i = 0; i < 52; i++) { 
    		$("#scoringmethod" + i).fadeOut();
    	}
    	$("#pointstext").fadeOut();
    	$(".tricks").fadeIn();
    }
    else {
    	for (i = 0; i < 52; i++) { 
    		showPointBox($("#include" + i), "false");
    	}
    	$("#pointstext").fadeIn();
    	$(".tricks").fadeOut();
    }
}

function showPointBox(element, toUpdate) {
	if ($(element).prop('checked') && $("#istrickcalling").val() == "false") {
    	$("#scoringmethod" + $(element).val()).fadeIn();
    }
    else {
    	$("#scoringmethod" + $(element).val()).fadeOut();
    }

    if (toUpdate == "true") {
	    var cardsUsed = 0;
	    var prev = $("#cardsinhand").val()
	    for (i = 0; i < 52; i++) { 
	    	if ($("#include" + i).prop('checked')) {
				cardsUsed += 1
			}
		}

		htmlCode = "";
		for (i = 1; i <= cardsUsed/4; i++) { 
			htmlCode += "<option value=\"" + i + "\">" + i + "</option>";
		}
		$("#cardsinhand")[0].innerHTML = htmlCode;

		if (prev <= cardsUsed/4) {
			$("#cardsinhand").val(prev);
		} else {
			$("#cardsinhand").val(Math.floor(cardsUsed/4));
		}
	}
}

function applyAll(element) {
	var num = element.value;
	$("#include"+ (parseInt(num))).prop('checked', element.checked)
	$("#include"+ (parseInt(num)+13)).prop('checked', element.checked)
	$("#include"+ (parseInt(num)+26)).prop('checked', element.checked)
	$("#include"+ (parseInt(num)+39)).prop('checked', element.checked)
	showPointBox($("#include" + (parseInt(num))), 'true');
	showPointBox($("#include" + (parseInt(num)+13)), 'true');
	showPointBox($("#include" + (parseInt(num)+26)), 'true');
	showPointBox($("#include" + (parseInt(num)+39)), 'true');
}
