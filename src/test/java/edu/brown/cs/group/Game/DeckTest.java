package edu.brown.cs.group.Game;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Deck;

/**
 * JUnit Tests for Deck.java class.
 */
public class DeckTest {

  @Test
  public void shuffleTest() {
    int different = 0;
    Deck deck1, deck2;
    deck2 = new Deck(true, new ArrayList<Integer>());
    for (int i = 0; i < 100; i++) {
      deck1 = new Deck(true, new ArrayList<Integer>());
      different += deck1.equals(deck2) ? 0 : 1;
      deck2 = deck1;
    }

    assertTrue(different > 98);
  }

  @Test
  public void equalsTest() {
    Deck deck1 = new Deck(true, new ArrayList<Integer>());
    Deck deck2 = new Deck(true, new ArrayList<Integer>());

  }

  @Test
  public void copyTest() {
    Deck deck1 = new Deck(true, new ArrayList<Integer>());
    Deck deck2 = deck1.copy();
    Deck deck3 = deck2.copy();
    Deck otherDeck = new Deck(true, new ArrayList<Integer>());
    assertTrue(deck1 != deck2);
    assertTrue(deck1.equals(deck2));
    assertTrue(deck1.equals(deck3));
    assertTrue(!deck1.equals(otherDeck));
  }

  @Test
  public void drawTest() {
    Deck deck1 = new Deck(true, new ArrayList<Integer>());
    Card prev = null;
    Card curr = null;
    int cardsDrawn = 0;
    while (!deck1.isEmpty()) {
      curr = deck1.draw();
      cardsDrawn++;
      if (prev != null) {
        assertTrue(curr != prev);
      }
      prev = curr;
    }
    assertTrue(cardsDrawn == 52);
  }
}
