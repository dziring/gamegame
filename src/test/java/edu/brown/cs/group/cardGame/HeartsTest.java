package edu.brown.cs.group.cardGame;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.brown.cs.group.cardGame.GameThread;
import edu.brown.cs.group.cardGame.Hearts;
import edu.brown.cs.group.cardGame.PlayerCard;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.player.HumanPlayer;
import edu.brown.cs.group.player.Player;

/**
 * JUnit Tests for Hearts.java class.
 */
public class HeartsTest {

  private Hearts hearts;
  private Map<Integer, Player> players;
  private Thread heartsThread;

  @Before
  public void setUp() throws Exception {
    // (Optional) Code to run before any tests begin goes here.
    players = new HashMap<Integer, Player>();
    players.put(0, new HumanPlayer("Dan", 0));
    players.put(1, new HumanPlayer("Samier", 1));
    players.put(2, new HumanPlayer("Carlos", 2));
    players.put(3, new HumanPlayer("Louisa", 3));
    hearts = new Hearts(players, "");
    hearts.startGame();
  }

  @After
  public void tearDown() throws InterruptedException {
    // heartsThread.join();
  }

  @Test
  public void isGameOverTest() {
    assertTrue(!hearts.isGameOver());
  }

  @Test
  public void correctEvaluationTest() {
    int playerWithTwoOfClubs = hearts.getWhoseTurn();
    int original = playerWithTwoOfClubs;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs), new Card(Rank.TWO, Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts
        .playCard(new PlayerCard(hearts.getPlayers().get(
            playerWithTwoOfClubs % 4), new Card(Rank.THREE,
            Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts
        .playCard(new PlayerCard(hearts.getPlayers().get(
            playerWithTwoOfClubs % 4), new Card(Rank.SEVEN,
            Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs % 4), new Card(Rank.FIVE, Suit.CLUBS)));
    assertTrue(hearts.getWhoseTurn() == ((original + 2) % 4));
  }

  @Test
  public void aceHighTest() {
    int playerWithTwoOfClubs = hearts.getWhoseTurn();
    int original = playerWithTwoOfClubs;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs), new Card(Rank.TWO, Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts
        .playCard(new PlayerCard(hearts.getPlayers().get(
            playerWithTwoOfClubs % 4), new Card(Rank.THREE,
            Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs % 4), new Card(Rank.ACE, Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs % 4), new Card(Rank.FIVE, Suit.CLUBS)));
    assertTrue(hearts.getWhoseTurn() == ((original + 2) % 4));
  }

  @Test
  public void firstRoundBadCardsTest() {
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(0), new Card(Rank.ACE, Suit.HEARTS))));
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(0), new Card(Rank.THREE, Suit.HEARTS))));
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(0), new Card(Rank.TWO, Suit.HEARTS))));
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(0), new Card(Rank.QUEEN, Suit.SPADES))));
  }

  @Test
  public void scoringTest() throws InterruptedException {
    int playerWithTwoOfClubs = hearts.getWhoseTurn();
    int original = playerWithTwoOfClubs;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs), new Card(Rank.TWO, Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts
        .playCard(new PlayerCard(hearts.getPlayers().get(
            playerWithTwoOfClubs % 4), new Card(Rank.THREE,
            Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs % 4), new Card(Rank.ACE, Suit.CLUBS)));
    playerWithTwoOfClubs++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(
        playerWithTwoOfClubs % 4), new Card(Rank.FIVE, Suit.CLUBS)));
    assertTrue(hearts.getWhoseTurn() == ((original + 2) % 4));

    int originalWinner = hearts.getWhoseTurn();
    int winner = hearts.getWhoseTurn();
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(winner),
        new Card(Rank.TWO, Suit.SPADES)));
    winner++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(winner % 4),
        new Card(Rank.THREE, Suit.SPADES)));
    winner++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(winner % 4),
        new Card(Rank.QUEEN, Suit.SPADES)));
    winner++;
    hearts.playCard(new PlayerCard(hearts.getPlayers().get(winner % 4),
        new Card(Rank.FIVE, Suit.SPADES)));
    Thread.sleep(200);
    assertTrue(hearts.getPlayers().get((originalWinner + 2) % 4).getScore() == 13);
  }

  @Test
  public void wrongPlayerTest() {
    int personWithTwoClubs = hearts.getWhoseTurn();
    assertTrue(hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(personWithTwoClubs), new Card(Rank.TWO, Suit.CLUBS))));

    // same player
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get(personWithTwoClubs), new Card(Rank.THREE, Suit.CLUBS))));

    // out of turn player
    assertTrue(!hearts.playCard(new PlayerCard(hearts.getPlayers()
        .get((personWithTwoClubs + 2) % 4), new Card(Rank.TWO, Suit.DIAMONDS))));
  }
}
