package edu.brown.cs.group.cardGame;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.player.HumanPlayer;
import edu.brown.cs.group.player.Player;

public class SetbackTest {
  private Setback setback;
  private Map<Integer, Player> players;

  @Before
  public void setUp() throws Exception {
    players = new HashMap<Integer, Player>();
    players.put(0, new HumanPlayer("Dan", 0));
    players.put(1, new HumanPlayer("Samier", 1));
    players.put(2, new HumanPlayer("Carlos", 2));
    players.put(3, new HumanPlayer("Louisa", 3));
    setback = new Setback(players, "");
    setback.setCardsInHand(1);
    setback.startGame();
  }

  @After
  public void tearDown() throws InterruptedException {
  }

  @Test
  public void isGameOverTest() {
    assertTrue(!setback.isGameOver());
  }

  @Test
  public void correctEvaluationTestWithAllTrump() {
    assertTrue(setback.callTricks(setback.getPlayers().get(0), 0));
    assertTrue(setback.callTricks(setback.getPlayers().get(1), 1));
    assertTrue(setback.callTricks(setback.getPlayers().get(2), 2));
    assertTrue(setback.callTricks(setback.getPlayers().get(3), 3));
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(3), new Card(Rank.THREE, Suit.CLUBS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(0), new Card(Rank.SEVEN, Suit.CLUBS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(1), new Card(Rank.TWO, Suit.CLUBS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(2), new Card(Rank.FIVE, Suit.CLUBS))) == true);
    assert (setback.getTrump().equals(Card.Suit.CLUBS));
    assertTrue(setback.getWhoseTurn() == 0);
  }

  @Test
  public void correctEvaluationTestWithNotAllTrump() {
    assert (setback.callTricks(setback.getPlayers().get(0), 0) == true);
    assert (setback.callTricks(setback.getPlayers().get(1), 1) == true);
    assert (setback.callTricks(setback.getPlayers().get(2), 2) == true);
    assert (setback.callTricks(setback.getPlayers().get(3), 3) == true);
    for (Card card : setback.getPlayers().get(0).getHand()) {
      if (card.getSuit().equals(Suit.HEARTS)) {
        setback.getPlayers().get(0).removeCard(card);
      }
    }
    if (setback.getPlayers().get(0).getHand().isEmpty()) {
      setback.getPlayers().get(0)
          .addCard(new Card(Card.Rank.EIGHT, Card.Suit.CLUBS));
    }
    assert (!setback.getPlayers().get(0).getHand().isEmpty());
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(3), new Card(Rank.TWO, Suit.HEARTS))) == true);
    assert (setback.getTrump().equals(Card.Suit.HEARTS));
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(0), new Card(Rank.THREE, Suit.CLUBS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(1), new Card(Rank.FOUR, Suit.HEARTS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(2), new Card(Rank.TEN, Suit.HEARTS))) == true);
    assertTrue(setback.getWhoseTurn() == 2);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(2), new Card(Rank.TWO, Suit.DIAMONDS))) == true);
    assert (setback.getTrump().equals(Card.Suit.HEARTS));
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(3), new Card(Rank.SEVEN, Suit.DIAMONDS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(0), new Card(Rank.FOUR, Suit.DIAMONDS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(1), new Card(Rank.TEN, Suit.HEARTS))) == true);
    assertTrue(setback.getWhoseTurn() == 1);
  }

  @Test
  public void correctEvaluationTestWithNoTrump() {
    assert (setback.callTricks(setback.getPlayers().get(0), 0) == true);
    assert (setback.callTricks(setback.getPlayers().get(1), 1) == true);
    assert (setback.callTricks(setback.getPlayers().get(2), 2) == true);
    assert (setback.callTricks(setback.getPlayers().get(3), 3) == true);
    for (Card card : setback.getPlayers().get(0).getHand()) {
      if (card.getSuit().equals(Suit.HEARTS)) {
        setback.getPlayers().get(0).removeCard(card);
      }
    }
    if (setback.getPlayers().get(0).getHand().isEmpty()) {
      setback.getPlayers().get(0)
          .addCard(new Card(Card.Rank.EIGHT, Card.Suit.CLUBS));
    }
    assert (!setback.getPlayers().get(0).getHand().isEmpty());
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(3), new Card(Rank.TWO, Suit.HEARTS))) == true);
    assert (setback.getTrump().equals(Card.Suit.HEARTS));
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(0), new Card(Rank.THREE, Suit.CLUBS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(1), new Card(Rank.FOUR, Suit.HEARTS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(2), new Card(Rank.TEN, Suit.HEARTS))) == true);
    assertTrue(setback.getWhoseTurn() == 2);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(2), new Card(Rank.TWO, Suit.DIAMONDS))) == true);
    assert (setback.getTrump().equals(Card.Suit.HEARTS));
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(3), new Card(Rank.SEVEN, Suit.DIAMONDS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(0), new Card(Rank.ACE, Suit.DIAMONDS))) == true);
    assert (setback.playCard(new PlayerCard(setback.getPlayers()
        .get(1), new Card(Rank.TEN, Suit.DIAMONDS))) == true);
    assertTrue(setback.getWhoseTurn() == 0);
  }

  @Test
  public void aceHighTest() {
    assert (setback.callTricks(setback.getPlayers().get(0), 0) == true);
    assert (setback.callTricks(setback.getPlayers().get(1), 1) == true);
    assert (setback.callTricks(setback.getPlayers().get(2), 2) == true);
    assert (setback.callTricks(setback.getPlayers().get(3), 3) == true);
    setback.playCard(new PlayerCard(setback.getPlayerList().get(3),
        new Card(Rank.TWO, Suit.CLUBS)));
    setback.playCard(new PlayerCard(setback.getPlayerList().get(0),
        new Card(Rank.THREE, Suit.CLUBS)));
    setback.playCard(new PlayerCard(setback.getPlayerList().get(1),
        new Card(Rank.ACE, Suit.CLUBS)));
    setback.playCard(new PlayerCard(setback.getPlayerList().get(2),
        new Card(Rank.FIVE, Suit.CLUBS)));
    assertTrue(setback.getWhoseTurn() == 1);
  }

}
