package edu.brown.cs.group.cardGame;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.brown.cs.group.cardGame.GameThread;
import edu.brown.cs.group.cardGame.PlayerCard;
import edu.brown.cs.group.cardGame.Spades;
import edu.brown.cs.group.games.Card;
import edu.brown.cs.group.games.Card.Rank;
import edu.brown.cs.group.games.Card.Suit;
import edu.brown.cs.group.player.HumanPlayer;
import edu.brown.cs.group.player.Player;

/**
 * JUnit Tests for Spades.java class.
 */
public class SpadesTest {

  private Spades spades;
  private Map<Integer, Player> players;
  private Thread spadesThread;

  @Before
  public void setUp() throws Exception {
    // (Optional) Code to run before any tests begin goes here.
    players = new HashMap<Integer, Player>();
    players.put(0, new HumanPlayer("Dan", 0));
    players.put(1, new HumanPlayer("Samier", 1));
    players.put(2, new HumanPlayer("Carlos", 2));
    players.put(3, new HumanPlayer("Louisa", 3));
    spades = new Spades(players, "");
    spades.startGame();
  }

  @After
  public void tearDown() throws InterruptedException {
    // heartsThread.join();
  }

  @Test
  public void isGameOverTest() {
    assertTrue(!spades.isGameOver());
  }

  @Test
  public void correctEvaluationTest() {
    assertTrue(spades.callTricks(spades.getPlayers().get(0), 0));
    assertTrue(spades.callTricks(spades.getPlayers().get(1), 1) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(2), 2) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(3), 3) == true);
    spades.playCard(new PlayerCard(spades.getPlayers().get(0), new Card(
        Rank.THREE, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(1), new Card(
        Rank.SEVEN, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(2), new Card(
        Rank.TWO, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(3), new Card(
        Rank.FIVE, Suit.CLUBS)));
    assertTrue(spades.getWhoseTurn() == 1);
  }

  @Test
  public void aceHighTest() throws InterruptedException {
    assertTrue(spades.callTricks(spades.getPlayers().get(0), 0));
    assertTrue(spades.callTricks(spades.getPlayers().get(1), 1) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(2), 2) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(3), 3) == true);
    spades.playCard(new PlayerCard(spades.getPlayers().get(0), new Card(
        Rank.FOUR, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(1), new Card(
        Rank.THREE, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(2), new Card(
        Rank.ACE, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(3), new Card(
        Rank.FIVE, Suit.CLUBS)));
    assertTrue(spades.getWhoseTurn() == 2);
  }

  @Test
  public void spadesTrumpTest() {
    assertTrue(spades.callTricks(spades.getPlayers().get(0), 0));
    assertTrue(spades.callTricks(spades.getPlayers().get(1), 1) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(2), 2) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(3), 3) == true);
    for (Card card : spades.getPlayers().get(1).getHand()) {
      if (card.getSuit().equals(Suit.CLUBS)) {
        spades.getPlayers().get(1).removeCard(card);
      }
    }
    spades.playCard(new PlayerCard(spades.getPlayers().get(0), new Card(
        Rank.ACE, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(1), new Card(
        Rank.TWO, Suit.SPADES)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(2), new Card(
        Rank.KING, Suit.CLUBS)));
    spades.playCard(new PlayerCard(spades.getPlayers().get(3), new Card(
        Rank.QUEEN, Suit.CLUBS)));
    assertTrue(spades.getWhoseTurn() == 1);
  }

  @Test
  public void wrongPlayerTest() {
    assertTrue(spades.callTricks(spades.getPlayers().get(0), 0));
    assertTrue(spades.callTricks(spades.getPlayers().get(1), 1) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(2), 2) == true);
    assertTrue(spades.callTricks(spades.getPlayers().get(3), 3) == true);
    assertTrue(spades.playCard(new PlayerCard(spades.getPlayers().get(0),
        new Card(Rank.ACE, Suit.SPADES))));

    // same player
    assertTrue(!spades.playCard(new PlayerCard(spades.getPlayers().get(0),
        new Card(Rank.THREE, Suit.CLUBS))));

    // out of turn player
    assertTrue(!spades.playCard(new PlayerCard(spades.getPlayers().get(3),
        new Card(Rank.TWO, Suit.DIAMONDS))));
  }
}
